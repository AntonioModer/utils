--- Geometry debugger
--- To be used for algorithm visualizations

local sprite = Sprite()
local canvas = sprite.canvas
-- active flag
local show = true
local queue = {}
local white = Color(255, 255, 255)
local color = white
local alpha = 1
local thickness = 1
local step = 0

local gd = {}

-- enable the visuals
function gd.show()
  show = true
  gd.redraw()
end

-- disable the visuals
function gd.hide()
  show = false
  canvas:clear()
end

-- reset the canvas right away
function gd.clear()
  canvas:clear()
  for i = #queue, 1, -1 do
    queue[i] = nil
  end
  color = white
  alpha = 1
  thickness = 1
  step = 0
end

-- redraw visuals
local dm = DisplayMode()
function gd.redraws(nsteps)
  if not show then
    return
  end
  if not display:get_mode(dm) then
    return
  end

  local vp = display.viewport
  vp:add_child(sprite)
  sprite:set_scale(vp.scalex, vp.scaley)
  sprite.depth = -math.huge
  
  canvas:clear()
  local i = 0
  while nsteps > 0 do
    -- process queue
    i = i + 1
    nsteps = nsteps - 1
    local q = queue[i]
    if q == nil then
      break
    end
    local t = q.t
    if t == "erase" then
      canvas:clear()
      -- clear queue up to the current step
      for k = 1, i do
        table.remove(queue, 1)
      end
      i = 0
      step = 0
    elseif t == "pause" then
      nsteps = nsteps - q.p
    elseif t == "alpha" then
      alpha = q.p
      canvas:set_fill_style(color, alpha)
      canvas:set_line_style(thickness, color, alpha)
    elseif t == "color" then
      color = q.p
      canvas:set_fill_style(color, alpha)
      canvas:set_line_style(thickness, color, alpha)
    elseif t == "thickness" then
      thickness = q.p
      canvas:set_line_style(thickness, color, alpha)
    elseif t == "point" then
      canvas:move_to(q.x, q.y)
      canvas:square(thickness)
      canvas:fill()
    elseif t == "line" then
      local dx, dy = q.x2 - q.x, q.y2 - q.y
      local d = math.sqrt(dx*dx + dy*dy)
      local cd = math.ceil(d)
      if nsteps >= cd then
        canvas:move_to(q.x, q.y)
        canvas:line_to(q.x2, q.y2)
        canvas:stroke()
        nsteps = nsteps - cd
      else
        local nx, ny = dx/d, dy/d
        canvas:move_to(q.x, q.y)
        canvas:rel_line_to(nx*nsteps, ny*nsteps)
        canvas:stroke()
        nsteps = 0
      end
    elseif t == "triangle" then
      canvas:move_to(q.x, q.y)
      canvas:line_to(q.x2, q.y2)
      canvas:line_to(q.x3, q.y3)
      canvas:fill()
    end
  end
end

function gd.redraw(nsteps)
  step = step + nsteps
  gd.redraws(step)
end

-- set the color
function gd.color(p)
  queue[#queue + 1] = { t = "color", p = p }
end

-- set the alpha
function gd.alpha(p)
  queue[#queue + 1] = { t = "alpha", p = p }
end

-- set the thickness
function gd.thickness(p)
  queue[#queue + 1] = { t = "thickness", p = p }
end

function gd.pause(p)
  queue[#queue + 1] = { t = "pause", p = p }
end

-- one or more points
function gd.point(x, y, ...)
  if type(x) == "table" then
    for i = 1, #x, 2 do
      gd.point(x[i], x[i + 1])
    end
  else
    if x and y then
      queue[#queue + 1] = { t = "point", x = x, y = y }
    end
    if select('#', ...) > 0 then
      gd.point(...)
    end
  end
end

-- filled triangle
function gd.triangle(x,y, x2,y2, x3,y3, ...)
  if type(x) == "table" then
    for i = 1, #x, 6 do
      gd.triangle(x[i],x[i + 1], x[i + 2],x[i + 3], x[i + 4],x[i + 5])
    end
  else
    if x and y and x2 and y2 and x3 and y3 then
      queue[#queue + 1] = { t = "triangle", x = x, y = y, x2 = x2, y2 = y2, x3 = x3, y3 = y3 }
    end
    local n = select('#', ...)
    if n > 0 then
      gd.triangle(...)
    end
  end
end

-- line or a polyline
function gd.line(x, y, x2, y2, ...)
  if type(x) == "table" then
    for i = 1, #x - 2, 2 do
      gd.line(x[i], x[i + 1], x[i + 2], x[i + 3])
    end
  else
    assert(x and y and x2 and y2, "line requires at least four parameters")
    queue[#queue + 1] = { t = "line", x = x, y = y, x2 = x2, y2 = y2 }
    if select('#', ...) > 0 then
      gd.line(x2, y2, ...)
    end
  end
end

-- line or closed polyline
function gd.loop(x, y, x2, y2, ...)
  if type(x) == "table" then
    for i = 1, #x - 2, 2 do
      gd.loop(x[i], x[i + 1], x[i + 2], x[i + 3])
    end
    gd.line(x[#x - 1], x[#x], x[1], x[2])
  else
    gd.line(x, y, x2, y2, ...)
    if select('#', ...) > 0 then
      local q = queue[#queue]
      gd.line(q.x2, q.y2, x, y)
    end
  end
end

-- clear
function gd.erase()
  queue[#queue + 1] = { t = "erase" }
end

--[[
-- pause
function gd.pause(t)
  assert(type(t == "number"), "pause period must be a number")
  queue[#queue + 1] = { t = "pause", s = t }
end
]]

return gd