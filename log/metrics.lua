metrics = {}

metrics.file = nil
metrics.attempts = {}
metrics.messages = {}
metrics.attempt = nil

metrics.begin_session = function(path)
  assert(metrics.file == nil, "Metrics session already started")

  -- filenames are formatted like: YYYYMMDDHHMMSS
  -- so that we don't have to parse metrics files
  -- just to find out WHEN they were created
  local ts = os.date("%Y%m%d%H%M%S")
  local fn = string.format("%s/%s", path, ts)
  metrics.file = io.open(fn, "w")
end
metrics.end_session = function()
  if metrics.file then
    metrics.file:close()
    metrics.file = nil
  end
end

metrics.log = function(sz, ...)
  sz = string.format(sz, ...)
  table.insert(metrics.messages, sz)
  if metrics.file then
    --local t = math.ceil(os.clock()*1000)
    --metrics.file:write(t)
    --metrics.file:write(": ")
    metrics.file:write(sz)
    metrics.file:write('\n')
    metrics.file:flush()
  end
end

metrics.begin_attempt = function()
  metrics.attempt = {}
  table.insert(metrics.attempts, metrics.attempt)
end
metrics.end_attempt = function()
  metrics.attempt = nil
end

metrics.add = function(p, value)
  local a = metrics.attempt
  if a then
    a[p] = (a[p] or 0) + value
  end
end
metrics.set = function(p, value)
  local a = metrics.attempt
  if a then
    a[p] = value
  end
end
metrics.get = function(p)
  local a = metrics.attempt
  if a then
    return a[p]
  end
end

-- return metrics files in a directory
metrics.get_filelist = function(dir, uploadtime)
  -- parse date and time from string
  local pattern = "(%d%d%d%d)(%d%d)(%d%d)(%d%d)(%d%d)(%d%d)"
  local Y, m, d, H, M, S = string.match(uploadtime, pattern)
  if not (Y2 and m2 and d2 and H2 and M2 and S2) then
    Y2, m2, d2 = "1970", "01", "01"
    H2, M2, S2 = "00", "00", "01"
  end
  local a1, a2 = tonumber(Y .. m .. d), tonumber(H .. M .. S)

  -- scan
  local files = {}
  for file in lfs.dir(dir) do
    local full = string.format("%s/%s", dir, file)
    local attr = lfs.attributes(full, "mode")
    if attr == "file" then
      -- parse date and time from filename
      local Y2, m2, d2, H2, M2, S2 = string.match(file, pattern)
      if not (Y2 and m2 and d2 and H2 and M2 and S2) then
        Y2, m2, d2 = "1970", "01", "01"
        H2, M2, S2 = "00", "00", "01"
      end

      -- was the file modified before the last upload?
      local b1, b2 = tonumber(Y2 .. m2 .. d2), tonumber(H2 .. M2 .. S2)
      if b1 > a1 or (b1 == a1 and b2 >= a2) then
        table.insert(files, file)
      end
    end
  end
  return files
end

metrics.upload_file = function(url, dir, file, version)
  -- todo: dir should not end in "/"
  local path = string.format("%s/%s", dir, file)
  local f = io.open(path, "r")
  if f == nil then
    return false, "could not open file"
  end
  -- copy the file to string
  local sz = f:read("*a")
  f:close()
  if sz == nil then
    return false, "cloud not read file"
  end
  -- send the file contents via POST, pass the filename/version as GET
  local u = string.format("%s?filename=%s&version=%s", url, file, version)
  local r = {}
  local headers =
  {
    ["content-length"] = string.len(sz),
    ["content-type"] = "text/plain"
  }
  local out =
  {
    method = "POST",
    url = u,
    headers = headers,
    source = ltn12.source.string(sz),
    sink = ltn12.sink.table(r)
  }
  local b, c = http.request(out)

  -- check the status code
  if c ~= 200 then
    return false, "connection error"
  end
  -- get the request body
  local res = table.concat(r)
  if res ~= "OK" and res ~= "file already exists" then
    return false, "server error"
  end
  return true
end

