display:create("terminal test", 1000, 600, 32, true)

terminal = require('utils.log.terminal')

steam = require('utils.sworks.main')

steam.init()
steam.connect()

t = Timer()
t:start(16, true)
function t:on_tick()
  steam.update()
  local name, id = steam.getUser()
  --steam.setStatInt('bonus', 1)
  steam.saveStats()
  terminal.trace('isInitialized', sworks.isInitialized)
  terminal.trace('user', name)
  terminal.trace('userid', id)
  --terminal.print(steam.getStatInt('bonus'))
  terminal.print(steam.getAchievement('BONUS_ACHIEVEMENT'))
end