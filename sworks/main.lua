-- checks if the "steam_appid.txt" file exists
-- we could write the file at this point
local f = io.open("steam_appid.txt", "r")
local appid = nil
if f then
  appid = f:read("*all")
  appid = tonumber(appid)
  f:close()
end
-- the module cannot work without an app id
if appid == nil then
  return
end
--assert(appid and appid > 0, "invalid Steam application ID")

-- check if Steamworks is available
local r, e = pcall(require, 'sworks')
if r == false or e == nil then
  return
end

local lib = {}

--- Initializes Steamworks
function lib.init()
  if not sworks.isInitialized then
    sworks.init()
  end
end

--- Releases Steamworks
function lib.release()
  if sworks.isInitialized then
    sworks.shutdown()
  end
end

--- Connects to account
function lib.connect()
  if sworks.isInitialized and sworks.user == nil then
    -- authentication
    -- why would we need more than one reference in Lua?
    sworks.user = sworks.LocalSteamUser()
    -- lua crash without these bad boys
    function sworks.user:onAchievementStored(status)
    end
    function sworks.user:onStatsStored(isGroup, name, value, maxValue)
    end
    --[[
    sworks.board = sworks.LeaderBoard()
    function board:onFound(status)
      self.busy = false
      if status then
        self.connected = true
      else
        self.connected = false
        --self:on_error("board not found")
      end
    end
    function board:onDownloaded(status, entries)
      self.busy = false
      if status then
        -- re-format the downloaded entry table
        local list = {}
        --for i = 1, self:GetEntryCount() do
          -- todo: e is sometimes nil when DownloadEntriesForUsers
          --local e = entries[i]
        for _, e in ipairs(entries) do
          local u = sworks.SteamUser(e.userID)
          local entry =
          {
            -- todo userID is a userdata object
            userid = e.userID,
            name = u.name,
            rank = e.rank,
            score = e.score,
            country = nil
          }
          table.insert(list, entry)
        end
        -- callback
        --self:on_download(list)
      else
        --self:on_error("download failed")
      end
    end
    function board:onUploaded(status, score, scorechanged, rank, oldrank)
      self.busy = false
      if status then
        local lp = sworks.LocalSteamUser()
        local entry =
        {
          -- todo userID is a userdata object
          userid = lp:GetSteamID(),
          name = lp.name,
          rank = rank,
          score = score,
          country = lp:GetCountry() -- only available for local user
        }
        -- callback
        --self:on_upload({entry})
      else
        --self:on_error("upload failed")
      end
    end
    ]]
  end
end

--- Disconnects from account
function lib.disconnect()
  if sworks.isInitialized and sworks.user then
    sworks.user = nil
  end
end

--- Checks if Steamworks is connected
-- @return true if initialized and authenticated
function lib.isConnected()
  return sworks.isInitialized and sworks.user and sworks.user.isAuthenticated
end

--- Updates Steamworks
function lib.update()
  sworks.update()
end

--- Returns the username (UTF-8) and account id (ANSI)
--- Don't return the "LocalUser" object
--- in the event we want to add another backend
-- @return username and account id
function lib.getUser()
  if sworks.user then
    return sworks.user.name, sworks.user:GetSteamID()
  end
end

-- User stats

--- Request client to download all user stats. Always call first.
-- Note: function is not asynchronous.
function lib.loadStats()
  if sworks.user then
    return sworks.user:RequestStats()
  end
end

--- Store all local stat modifications on the server.
-- Don't notify the user before receiving notification from the server via:
function lib.saveStats()
  if sworks.user then
    return sworks.user:StoreStats()
  end
end

--- Once achievements or stats are available to the client
-- they can be modified locally.
function lib.setStat(t, k, v)
  if sworks.user == nil then
    return
  end
  if t == 'bool' then
    return sworks.user:SetAchievement(k)
  elseif t == 'int' then
    return sworks.user:SetStatInt(k, v)
  elseif t == 'float' then
    return sworks.user:SetStatFloat(k, v)
  end
end

function lib.getStat(t, k)
  if sworks.user == nil then
    return
  end
  local r, v
  if t == 'bool' then
    r, v = sworks.user:GetAchievement(k)
  elseif t == 'int' then
    r, v = sworks.user:GetStatInt(k)
  elseif t == 'float' then
    r, v = sworks.user:GetStatFloat(k)
  end
  if r == true then
    return v
  end
end

-- Leaderboards
--[[
function lib.setBoard(b)
  if lib.board == nil then
    return
  end
  if lib.board:Find(board) then
    lib.board.busy = true
  end
end

-- download entries in range (s)tart to (e)nd
function lib.loadScores(t, s, e)
  assert(s and e and s <= e, "invalid start and end values")
  assert(self.connected, "no board selected")
  assert(not self.busy, "busy")
  -- global, aroundUser, friends, users
  t = t or 'global'
  if lib.board:DownloadEntries(t, s, e) then
    lib.board.busy = true
  end
end

-- download entry for a particular user
function lib.loadScoresFor(uid)
  assert(self.connected, "no board selected")
  assert(not self.busy, "busy")
  if lib.board:DownloadEntriesForUsers({uid}) then
    lib.board.busy = true
  end
end

-- upload a new entry
function lib.saveScores(t, v)
  assert(self.connected, "no board selected")
  assert(not self.busy, "busy")
  -- replace, keepBest
  t = t or "keepBest"
  if lib.board:UploadScore(t, v, nil) then
    lib.board.busy = true
  end
end
]]
return lib