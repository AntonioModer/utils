hiscore = {}

hiscore.clients = {}
hiscore.boards = {}
hiscore.calls = {}

local board_api =
{
  "connect", "disconnect", "find", "find_or_create", "download", "download_entry", "upload"
}

hiscore.board = function(client)
  if hiscore.clients[client] == nil then
    return
  end
  local board = hiscore.clients[client].board()
  if hiscore.boards[client] == nil then
    hiscore.boards[client] = {}
  end
  -- queue board function calls
  for i, f in ipairs(board_api) do
    local fn = "_" .. f
    board[fn] = board[f]
    board[f] = function(board, ...)
      hiscore.queue(board, fn, ...)
    end
  end
  table.insert(hiscore.boards[client], board)
  return board
end

hiscore.pcall = function(board, func, ...)
  local r, e = pcall(func, board, ...)
  --func(board, ...)
  if r == false then
    -- trim line error
    local i, j = string.find(e, ":%d+:")
    while i and j do
      e = string.sub(e, j + 2)
      i, j = string.find(e, ":%d+:")
    end
    hiscore.calls[board] = {}
    board:on_error(e)
  end
end

hiscore.queue = function(board, func, ...)
  if hiscore.calls[board] == nil then
    hiscore.calls[board] = {}
  end
  table.insert(hiscore.calls[board], 1, { func, { ... } })
end

hiscore.update = function()
  for _, b in pairs(hiscore.boards) do
    for _, v in ipairs(b) do
      hiscore.pcall(v, v.update)
    end
  end
  for b, v in pairs(hiscore.calls) do
    if b:is_busy() == false and #v > 0 then
      local q = table.remove(v)
      local f = q[1]
      local p = q[2]
      assert(b[f], "missing function in queue: " .. f)
      hiscore.pcall(b, b[f], unpack(p))
    end
  end
end