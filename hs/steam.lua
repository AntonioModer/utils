assert(hiscore, "hiscore module required")

-- steamworks client
local r, sworks = pcall(require, 'sworks')

if r == false or sworks == nil then
  return nil
end

local steamc = {}

hiscore.clients.steam = steamc

function steamc.board()
  local board = sworks.LeaderBoard()
  -- have we found a board?
  board.connected = false
  -- busy with some operation?
  board.busy = false

  function board.onFound(self, status)
    self.busy = false
    if status then
      self.connected = true
    else
      self.connected = false
      self:on_error("board not found")
    end
  end
  function board.onDownloaded(self, status, entries)
    self.busy = false
    if status then
      -- re-format the downloaded entry table
      local list = {}
      --for i = 1, self:GetEntryCount() do
        -- todo: e is sometimes nil when DownloadEntriesForUsers
        --local e = entries[i]
      for _, e in ipairs(entries) do
        local u = sworks.SteamUser(e.userID)
        local entry =
        {
          -- todo userID is a userdata object
          userid = e.userID,
          name = u.name,
          rank = e.rank,
          score = e.score,
          country = ""
        }
        table.insert(list, entry)
      end
      
      -- callback
      self:on_download(list)
    else
      self:on_error("download failed")
    end
  end
  function board.onUploaded(self, status, score, scorechanged, rank, oldrank)
    self.busy = false
    if status then
      local lp = sworks.LocalSteamUser()
      local entry =
      {
        -- todo userID is a userdata object
        userid = lp:GetSteamID(),
        name = lp.name,
        rank = rank,
        score = score,
        country = lp:GetCountry() -- only available for local user
      }
      -- callback
      self:on_upload({entry})
    else
      self:on_error("upload failed")
    end
  end
  
  function board.get_localuser(self)
    local lp = sworks.LocalSteamUser()
    return lp.name, lp:GetSteamID()
  end
  function board.set_localuser(self, name)
    
  end

  -- currently busy?
  function board.is_busy(self)
    return self.busy
  end
  -- connect
  function board.connect(self, url)
  --[[
    -- check steam_appid.txt
    local f = io.open("steam_appid.txt", "r")
    local appid = nil
    if f then
      appid = f:read("*all")
      appid = tonumber(appid)
      f:close()
    end
    assert(appid, "invalid Steam application ID")
    ]]
    -- make sure the steam client is working
    assert(sworks.isInitialized, "Steamworks not initialized")
    -- make sure we have a working connection
    local lp = sworks.LocalSteamUser()
    assert(lp.isAuthenticated, "local user not authenticated")
  end
  -- disconnect
  function board.disconnect(self)
    assert(not self.busy, "busy")
    self.connected = false
  end
  -- connect and find a leaderboard
  function board.find(self, board)
    assert(not self.busy, "busy")
    if self:Find(board) then
      self.busy = true
    end
  end
  function board.find_or_create(self, board)
    assert(not self.busy, "busy")
    -- sortMethod: "ascend", "descend", "none"
    -- displayType: "none", "number", "secs", "msecs", "unknown"
    if self:FindOrCreate(board, "descend", "none") then
      self.busy = true
    end
  end
  -- download entries in range (s)tart to (e)nd
  function board.download(self, s, e)
    assert(s and e and s <= e, "invalid start and end values")
    assert(self.connected, "no board selected")
    assert(not self.busy, "busy")
    -- global, aroundUser, friends, users
    if self:DownloadEntries('global', s, e) then
      self.busy = true
    end
  end
  -- download entry for a particular user
  function board.download_entry(self, user)
    assert(self.connected, "no board selected")
    assert(not self.busy, "busy")
    --if user == nil then
      -- todo: get steamid from ANY username
      user = sworks.LocalSteamUser()
      user = user:GetSteamID()
    --end
    if self:DownloadEntriesForUsers({user}) then
      self.busy = true
    end
  end
  -- upload a new entry
  function board.upload(self, score, method)
    -- default method is "best"
    method = method or "best"
    assert(score == math.floor(score), "score must be an integer")
    assert(self.connected, "no board selected")
    assert(method == "best" or method == "replace", "unsupported upload method")
    assert(not self.busy, "busy")
    -- sworks uses "keepBest"
    if method == "best" then
      method = "keepBest"
    end
    -- keepBest, replace
    if self:UploadScore(method, score, nil) then
      self.busy = true
    end
  end
  -- update the board
  function board.update(self)
    sworks.update()
  end
  -- callback after a download is completed
  function board.on_download(self, entries)
  end
  -- callback after an upload is completed
  function board.on_upload(self, entries)
  end
  -- callback when an error occurs
  function board.on_error(self, status)
  end
  
  return board
end