assert(hiscore, "hiscore module required")

require('LuaXml')

local filec = {}

hiscore.clients.file = filec

filec.board = function()
  local board = {}

  board.get_localuser = function(self)
    return self.user, self.userid
  end
  board.set_localuser = function(self, name, id)
    self.user = name
    self.userid = id
  end
  
  -- currently busy?
  board.is_busy = function(self)
    return false
  end
  -- connect and find a leaderboard
  board.connect = function(self, file)
    assert(file, "file not specified")
    self.file = file
    self.boards = {}
    xml.tag(self.boards, "Boards")
    local x = xml.load(file)
    if x then
      local b = xml.find(x, "Boards")
      if b then
        for i = 1, #b do
          local board = b[i]
          self.boards[board.name] = board
        end
      end
    end
    for _, b in pairs(self.boards) do
      for i = 1, #b do
        local e = b[i]
        if xml.tag(e) == "Entry" then
          e.rank = tonumber(e.rank)
          e.score = tonumber(e.score)
        end
      end
    end
  end
  -- disconnect
  board.disconnect = function(self)
    self.file = nil
    self.boards = nil
  end
  -- select a board
  board.find = function(self, board)
    assert(self.file, "file not specified")
    assert(self.boards[board], "board not found")
    self.board = board
  end
  -- select a board
  board.find_or_create = function(self, board)
    assert(self.file, "file not specified")
    if self.boards[board] == nil then
      local b = { name = board }
      xml.tag(b, "Board")
      self.boards[board] = b
    end
    self.board = board
  end
  -- download entries in range (s)tart to (e)nd
  board.download = function(self, s, e)
    assert(s and e and s <= e, "invalid start and end values")
    assert(self.file, "file not specified")
    assert(self.board, "no board selected")
    local list = {}
    local b = self.boards[self.board]
    for i = s, e do
      table.insert(list, b[i])
    end
    self:on_download(list)
  end
  -- download entry for a particular user
  board.download_entry = function(self, user)
    assert(self.file, "file not specified")
    assert(self.board, "no board selected")
    assert(user ~= nil or self.user ~= nil, "user not specified")
    local u = user or self.user
    local list = {}
    local b = self.boards[self.board]
    for i, v in ipairs(b) do
      if b[i].name == u then
        table.insert(list, b[i])
      end
    end
    self:on_download(list)
  end
  -- upload a new entry
  board.upload = function(self, score, method)
    method = method or "best"
    assert(self.file, "file not specified")
    assert(self.board, "no board selected")
    assert(self.user, "user not specified")
    assert(score == math.floor(score), "score must be an integer")
    assert(method == "best" or method == "replace" or method == "insert", "unsupported upload method")
    local u = self.user
    local id = self.userid
    local e =
    {
      userid = id,
      name = u,
      score = score,
      time = os.date()
    }
    xml.tag(e, "Entry")
    local b = self.boards[self.board]
    if method == "insert" then
      -- insert new entry
      table.insert(b, e)
    elseif method == "replace" or method == "best" then
      -- replace existing entry
      local r = nil
      for i, entry in ipairs(b) do
        if entry.name == u then
          r = i
          break
        end
      end
      if r == nil then
        table.insert(b, e)
      else
        if method == "replace" or b[r].score <= e.score then
          b[r] = e
        else
          b[r].time = e.time
          e = b[r]
        end
      end
    end
    -- sort elements
    table.sort(b, function(y, z) return y.score > z.score end)
    -- update ranks
    for i = 1, #b do
      b[i].rank = i
    end
    xml.save(self.file, self.boards)
    self:on_upload({ e })
  end
  -- update the current state
  board.update = function(self)
  end
  -- callback after a download is completed
  board.on_download = function(self, entries)
  end
  -- callback after an upload is completed
  board.on_upload = function(self, entries)
  end
  -- callback when an error occurs
  board.on_error = function(self, status)
  end
  
  return board
end