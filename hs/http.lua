assert(hiscore, "hiscore module required")

-- php web client
local http = require("socket.http")
local md5 = require("md5")

require('LuaXml')

local httpc = {}

hiscore.clients.http = httpc

httpc.board = function()
  local board = {}
  local parse_xml_board = function(b)
    -- evaluate and parse
    local r, x = pcall(xml.eval, b)
    assert(r and x, "could not evaluate results")
    local b = xml.find(x, 'Board')
    assert(b, "no entries found")
    -- convert XML to list
    local list = {}
    for i = 1, #b do
      local r = b[i]
      local tag = xml.tag(r)
      if tag == "Entry" then
        local entry =
        {
          userid = r.userid,
          name = r.name,
          rank = tonumber(r.rank),
          score = tonumber(r.score),
          time = r.time,
          country = r.country
        }
        list[i] = entry
      end
    end
    return list
  end
  -- url encode
  local encode_sz = function(sz)
    local sz = string.gsub(sz, "\n", "\r\n")
    sz = string.gsub (sz, "([^%w %-%_%.%~])",
        function (c) return string.format ("%%%02X", string.byte(c)) end)
    sz = string.gsub (sz, " ", "+")
    return sz
  end

  board.get_localuser = function(self)
    return self.user, self.userid
  end
  board.set_localuser = function(self, name)
    self.user = name
  end
  
  -- currently busy?
  board.is_busy = function(self)
    return false
  end
  -- connect and find a leaderboard
  board.connect = function(self, url)
    -- url should point to the hiscore .PHP file
    self.url = url
    -- get userid (ip)
    local u = string.format("%s?ip=true", self.url)
    local b, c = http.request(u)
    assert(b and c and c == 200, "could not connect")
    self.userid = b
  end
  -- disconnect
  board.disconnect = function(self)
    self.url = nil
  end
  -- select a board
  board.find = function(self, board)
    self.board = board
    self.nb = "false"
  end
  -- select a board
  board.find_or_create = function(self, board)
    self.board = board
    -- create new board flag
    self.nb = "true"
    --[[
    local b = encode_sz(board)
    local u = string.format("%s?nb=%s", self.url, b)
    local r, c = http.request(u)
    assert(r and c and c == 200, "could not create board")
    ]]
  end
  -- download entries in range (s)tart to (e)nd
  board.download = function(self, s, e)
    assert(s and e and s <= e, "invalid start and end values")
    assert(self.url, "url not specified")
    assert(self.board, "no board selected")

    -- request a leaderboard
    local b = encode_sz(self.board)
    local u = string.format("%s?b=%s&s=%d&e=%d", self.url, b, s, e)
    local r, c = http.request(u)
    assert(r and c and c == 200, "download request failed")
    local list = parse_xml_board(r)
    self:on_download(list)
  end
  -- download entry for a particular user
  board.download_entry = function(self, user)
    assert(self.url, "url not specified")
    assert(self.board, "no board selected")
    assert(user or self.user, "user not specified")

    -- request a particular entry
    local b = encode_sz(self.board)
    local u = encode_sz(user)
    local p = string.format("%s?b=%s&n=%s", self.url, b, u)
    local r, c = http.request(p)
    assert(r and c and c == 200, "download request failed")
    local list = parse_xml_board(r)
    self:on_download(list)
  end
  -- upload a new entry
  board.upload = function(self, score, method)
    method = method or "best"
    assert(self.url, "url not specified")
    assert(self.board, "no board selected")
    assert(self.user, "user not specified")
    assert(score == math.floor(score), "score must be an integer")
    assert(method == "best" or method == "replace" or method == "insert", "unsupported upload method")

    -- checksum of the parameters
    local params = string.format("%s%s", self.user, score)
    local cs = md5.sumhexa(params)
    -- encode strings that may contain spaces or symbols
    local b = encode_sz(self.board)
    local u = encode_sz(self.user)
    -- create board flag
    local nb = self.nb
    -- perform the upload
    local p = string.format("b=%s&n=%s&s=%d&m=%s&cs=%s&nb=%s", b, u, score, method, cs, nb)
    local r, c = http.request(self.url, p)
    -- was the upload successful?
    assert(r and c and c == 200, "upload request failed")
    local list = parse_xml_board(r)
    self:on_upload(list)
  end
  -- update the current state
  board.update = function(self)
  end
  -- callback after a download is completed
  board.on_download = function(self, entries)
  end
  -- callback after an upload is completed
  board.on_upload = function(self, entries)
  end
  -- callback when an error occurs
  board.on_error = function(self, status)
  end
  
  return board
end