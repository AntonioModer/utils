local colors = require("utils.io.colors")

--
-- Bitmap file parser
--
local base = _G
local byte = string.byte
local insert = table.insert
local string = string
local io = io
local bmp = {}

-- Parse a 16-bit WORD from the binary string
local word = function(str, offset)
  local lo = byte(str, offset)
  local hi = byte(str, offset + 1)
  return hi*256 + lo
end

-- Parse a 32-bit DWORD from the binary string
local dword = function(str, offset)
  local lo = word(str, offset)
  local hi = word(str, offset + 2)
  return hi*65536 + lo
end

-- Open the BMP file and get header info
bmp.open = function(filename)
  local b = {}
  b.file = io.open(filename, "rb")
  if b.file == nil then
    return nil, "Could not open file:" .. filename
  end
  
  local header = b.file:read(55)
  if header == nil then
    b.file:close()
    return nil, "Could not read bitmap header"
  end
  
  -- BITMAPFILEHEADER (14 bytes)
  -- starts from the 1-st byte
  b.bfType = word(header, 1)
  b.bfSize = word(header, 3)
  -- 7: bfReserved1
  -- 8: bfReserved2
  b.bfOffBits = word(header, 11)
  
  if b.bfType ~= 0x4D42 then
    b.file:close()
    return nil, "Invalid bitmap magic value:" .. (b.bfType or "nil")
  end

  -- BITMAPINFOHEADER (40 bytes)
  -- starts from the 15-th byte
  b.biSize = dword(header, 15)
  -- dimensions of the bitmap
  b.biWidth = dword(header, 19)
  b.biHeight = dword(header, 23)
  b.biPlanes = word(header, 27)
  -- bit depth
  b.biBitCount = word(header, 29)
  b.biCompression = dword(header, 31)
  -- 35: biSizeImage
  -- 39: biXPelsPerMeter
  -- 43: biYPelsPerMeter
  b.biClrUsed = dword(header, 47)
  -- 51: biClrImportant

  -- RGBQUAD
  if b.biBitCount == 1 or b.biBitCount == 4 or b.biBitCount == 8 then
    local nc = b.biClrUsed
    if nc == 0 then
      nc = 2^b.biBitCount
    end
    
    b.file:seek("set", b.biSize + 15 - 1)
    local quads = b.file:read(nc*4)
  
    local offset = 0
    b.colors = {}
    -- color table information
    for i = 1, nc do
      local blue = byte(quads, offset + 1)
      local green = byte(quads, offset + 2)
      local red = byte(quads, offset + 3)
      -- offset + 4 is reserved
      local hex = colors.rgb_to_hex(red, green, blue)
      insert(b.colors, hex)
      offset = offset + 4
    end
  end
  
  -- bitmap width must be a multiple of 4
  b.biPadding = b.biWidth%4
  -- bytes per pixel
  b.biBPP = b.biBitCount/8
  
  return b
end

-- Close the io stream
bmp.close = function(b)
  if b.file then
    b.file:close()
    b.file = nil
  end
end

-- Parse the bits of an open BMP file
bmp.parse = function(b)
  if b.file == nil then
    return nil, "Could not parse file"
  end
  
  -- move file index to BMP bits
  b.file:seek("set", b.bfOffBits)

  if b.biCompression ~= 0 then
    return nil, "Unsupported bitmap compression:" .. b.biCompression
  end

  local data = {}
  local w, h = b.biWidth, b.biHeight
  local xpadding = b.biPadding
  local bpp = b.biBPP
  local csize = w*h*bpp + xpadding*h
  local bytes = b.file:read(csize)
  if bytes == nil then
    return nil, "Could not read bitmap data"
  end

    -- 32 bit:
    -- X8 R8 G8 B8
    -- A8 R8 G8 B8
  if b.biBitCount == 24 then
    -- 24 BIT: R8 G8 B8
    -- 3 bytes = 1 pixel
    for y = 0, h - 1 do
      for x = 0, w - 1 do
        local o = y*w*bpp + y*xpadding + x*bpp
        local red = byte(bytes, o + 3)
        local green = byte(bytes, o + 2)
        local blue = byte(bytes, o + 1)
        local hex = colors.rgb_to_hex(red, green, blue)
        insert(data, hex)
      end
    end
    -- 16 bit:
    -- R5 G6 B5
    -- X1 R5 G5 B5
    -- X4 R4 G4 B4
    -- A1 R5 G5 B5
    -- A4 R4 G4 B4
  elseif b.biBitCount == 8 then
    -- 8 bit: 256 colors from color table
    -- 1 byte = 1 pixel
    -- each pixel references an index in the color table
    for y = 0, h - 1 do
      for x = 0, w - 1 do
        local o = y*w + y*xpadding + x
        local i = byte(bytes, o + 1)
        local hex = b.colors[i + 1]
        insert(data, hex)
      end
    end
  elseif b.biBitCount == 4 then
    -- 4 bit: 16 colors from color table
    for y = 0, h - 1 do
      for x = 0, w - 1, 2 do
        local o = y*(w/2) + y*xpadding + (x/2)
        local i = byte(bytes, o + 1)
        local lo = i%16
        local hi = (i - lo)/16
        insert(data, b.colors[hi + 1])
        insert(data, b.colors[lo + 1])
      end
    end
  elseif b.biBitCount == 1 then
    -- 1 bit: 2 colors from color table
    for y = 0, h - 1 do
      for x = 0, w - 1, 8 do
        local o = y*(w/8) + y*xpadding + (x/8)
        local i = byte(bytes, o + 1)
        for j = 8, 1, -1 do
          local lo = i%(j + 1)
          local hi = base.math.floor(lo/j)
          insert(data, b.colors[hi + 1])
        end
      end
    end
  else
    return nil, "Unsupported bitmap depth:" .. b.biBitCount
  end
  b.data = data
  
  return data
end

return bmp