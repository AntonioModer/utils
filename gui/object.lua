local base = _G
module("gui")

-- Base object class using sprite
-- 0, 0 is the top left corner of the object
-- manually flips the y axis so it points south

ObjectG = {}
ObjectGMT = { __index = ObjectG }

ObjectG.create = function(x, y, width, height, mt)
  local self = {}
  base.setmetatable(self, mt or ObjectGMT)

  self.x = x
  self.y = y
  self.width = width
  self.height = height
  self.sprite = base.Sprite(x, -y) -- flip y axis
  self.node = self.sprite

  return self
end

ObjectG.destroy = function(self)
  self.x = nil
  self.y = nil
  self.width = nil
  self.height = nil
  self.sprite = nil
  self.node = nil
end

-- parent reference
ObjectG.set_parent = function(self, parent)
  self.parent = parent
end

ObjectG.get_parent = function(self)
  return self.parent
end

-- style accessors
ObjectG.set_style = function(self, style, value)
  self.styles[style] = value
end

ObjectG.set_color = function(self, style, value)
  local c
  if value then
    c = base.Color(value)
  end
  self.colors[style] = c
end

-- dimensions accessors
ObjectG.get_position = function(self)
  return self.x, self.y
end

ObjectG.set_position = function(self, x, y)
  self.x = x
  self.y = y
  self.node:set_position(x, -y) -- flip y axis
end

ObjectG.set_size = function(self, w, h)
  self.width = w
  self.height = h
end

ObjectG.get_size = function(self)
  return self.width, self.height
end

-- transforms point from parent to local coords
ObjectG.parent_to_local = function(self, x, y)
  -- todo: rotated elements
  return x - self.x, y - self.y
end

-- checks local point for intersection
ObjectG.test_point = function(self, x, y)
  local l, t, r, b = 0, 0, self.width, self.height
  return not(x < l or x > r or y < t or y > b)
end

-- input events
ObjectG.write = function(self, c)
  -- character input
end

ObjectG.key_command = function(self, key, ctrl, shift)
  -- key input
end

ObjectG.key_press = function(self, key)
  -- key down
end

ObjectG.key_release = function(self, key)
  -- key up
end

ObjectG.mouse_press = function(self, button, x, y)
  -- mouse button down
end

ObjectG.mouse_release = function(self, button, x, y)
  -- mouse button up
end

ObjectG.mouse_move = function(self, x, y, x2, y2)
  -- mouse cursor move
end

ObjectG.wheel_move = function(self, z)
  -- mouse wheel move
end

ObjectG.begin_drag = function(self, button, x, y)

end

ObjectG.dragging = function(self, button, x, y)
  -- mouse move while holding button down
end

ObjectG.end_drag = function(self, button, x, y)

end

ObjectG.cancel_drag = function(self, button, x, y)
  -- pressed another mouse button during drag
end

ObjectG.single_click = function(self, button, x, y)

end

ObjectG.double_click = function(self, button, x, y)

end

ObjectG.redraw = function(self, delta)

end

ObjectG.update = function(self, delta)

end

Object = ObjectG.create