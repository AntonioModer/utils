local mouse = mouse
local base = _G
module("gui")

local mbuffer = nil
local time_mpress = nil

local mouse_event = function(t, x, y, b)
  local e = { type = t, x = x, y = y, button = b }
  base.table.insert(mbuffer, 1, e)
  -- keep the buffer at a reasonable size
  if #mbuffer > 20 then
    base.table.remove(mbuffer)
  end
end

local mouse_on_press = function(mouse, button)
  -- get the mouse position in container coords
  local x, y = get_mouse_position()
  input_event('mouse_press', button, x, y)

  if mbuffer == nil then
    return
  end
  
  -- canceled drag
  local l1 = mbuffer[1]
  if l1 and l1.type == 'move' then
    local l2 = mbuffer[2]
    if l2 and l2.type == 'press' then
      input_event('cancel_drag', button, x, y)
    end
  end

  if mbuffer == nil then
    return
  end
  
  -- double click/tap
  if time_mpress < get_style('double_click') then
    if get_style('touchscreen') == 'on' then
      -- touchscreen
      input_event('double_click', button, x, y)
    else
      -- non-touchscreen
      local l1 = mbuffer[1]
      if l1 and l1.type == 'release' and button == l1.button then
        local l2 = mbuffer[2]
        if l2 and l2.type == 'press' and button == l2.button then
          input_event('double_click', button, x, y)
        end
      end
    end
  end

  if mbuffer == nil then
    return
  end
  
  mouse_event('press', x, y, button)

  time_mpress = 0
  if old_mouse_on_press then
    old_mouse_on_press(mouse, button)
  end
end

local mouse_on_release = function(mouse, button)
  -- get the mouse position in container coords
  local x, y = get_mouse_position()
  input_event('mouse_release', button, x, y)
  
  if mbuffer == nil then
    return
  end

  -- single click/tap
  if time_mpress < get_style('single_click') then
    if get_style('touchscreen') == 'on' then
      -- touchscreens: ignore short swipes
      local swipe = false
      local l1 = mbuffer[1]
      if l1 and l1.type == 'move' then
        local dist = l1.x*l1.x + l1.y*l1.y
        if base.math.sqrt(dist) > get_style('swipe_threshold') then
          swipe = true
        end
      end
      if swipe == false then
        input_event('single_click', button, x, y)
      end
    else
      -- non-touchscreen
      local l1 = mbuffer[1]
      if l1 and l1.type == 'press' and button == l1.button then
        input_event('single_click', button, x, y)
      end
    end
  end

  if mbuffer == nil then
    return
  end

  -- canceled drag/end drag
  local l1 = mbuffer[1]
  local l2 = mbuffer[2]
  if l1 and l1.type == 'move' then
    if l2 and l2.type == 'press' then
      if button == l2.button then
        input_event('end_drag', button, x, y)
      else
        input_event('cancel_drag', button, x, y)
      end
    end
  end

  if mbuffer == nil then
    return
  end

  mouse_event('release', x, y, button)
  if old_mouse_on_release then
    old_mouse_on_release(mouse, button)
  end
end

local mouse_on_move = function(mouse, dx, dy)
  -- get the mouse position in container coords
  local x, y = get_mouse_position()
  -- Y pointing south, old version: y - dy
  input_event('mouse_move', x, y, x - dx, y + dy)

  if mbuffer == nil then
    return
  end
  
  -- begin drag/dragging
  local l1 = mbuffer[1]
  if l1 and l1.type == 'press' then
    -- Y pointing south, old version: y - dy
    input_event('begin_drag', l1.button, x - dx, y + dy)

    input_event('dragging', l1.button, x, y)
  end

  if mbuffer == nil then
    return
  end

  local l2 = mbuffer[2]
  if l1 and l1.type == 'move' then
    if l2 and l2.type == 'press' then
      input_event('dragging', l2.button, x, y)
    end
  end
  if l1 and l1.type == 'move' then
    l1.x = l1.x + dx
    l1.y = l1.y + dy
  else
    mouse_event('move', dx, dy)
  end
  
  if old_mouse_on_move then
    old_mouse_on_move(mouse, dx, dy)
  end
end

local mouse_on_wheelmove = function(mouse, z)
  input_event('wheel_move', z)
  if old_mouse_on_wheelmove then
    old_mouse_on_wheelmove(mouse, z)
  end
end

-- returns the mouse position in default container coords
get_mouse_position = function()
  if mouse then
    local w, h = container:get_size()
    local x = mouse.xaxis + w/2
    local y = -(mouse.yaxis - h/2)
    return x, y
  end
end

-- snatch callbacks from user
local grab_mouse = function()
  if mouse == nil then
    return
  end
  if mouse.on_press ~= mouse_on_press then
    old_mouse_on_press = mouse.on_press
    mouse.on_press = mouse_on_press
  end
  if mouse.on_release ~= mouse_on_release then
    old_mouse_on_release = mouse.on_release
    mouse.on_release = mouse_on_release
  end
  if mouse.on_move ~= mouse_on_move then
    old_mouse_on_move = mouse.on_move
    mouse.on_move = mouse_on_move
  end
  if mouse.on_wheelmove ~= mouse_on_wheelmove then
    old_mouse_on_wheelmove = mouse.on_wheelmove
    mouse.on_wheelmove = mouse_on_wheelmove
  end
end
local drop_mouse = function()
  if mouse == nil then
    return
  end
  mouse.on_press = old_mouse_on_press
  mouse.on_release = old_mouse_on_release
  mouse.on_move = old_mouse_on_move
  mouse.on_wheelmove = old_mouse_on_wheelmove
  old_mouse_on_press = nil
  old_mouse_on_release = nil
  old_mouse_on_wheelmove = nil
end

init_mouse = function()
  mbuffer = {}
  time_mpress = 0
  
  grab_mouse()
end
release_mouse = function()
  mbuffer = nil
  time_mpress = nil
  
  drop_mouse()
end

update_mouse = function(dt)
  grab_mouse()
  -- mouse press time
  if time_mpress then
    time_mpress = time_mpress + dt
  end
end