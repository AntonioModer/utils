local base = _G
module("gui")

local styles = {}
local colors = {}

-- styles initialization
function init_styles()
  -- default font object
  styles['font'] = base.Font()
  styles['font']:load_system('Verdana', 8)
  styles['line_spacing'] = 16
  
  -- wait period until repeat is on, in seconds
  styles['repeat_delay'] = 0.5
  -- keystroke repeat rate, in seconds
  styles['repeat_rate'] = 0.05
  styles['cursor_blink'] = 0.5
  styles['single_click'] = 0.3
  styles['double_click'] = 0.6
  -- reports touchscreen taps as clicks
  styles['touchscreen'] = 'off'
  -- used for touchscreen taps, in pixels
  styles['swipe_threshold'] = 25
  -- text alignment
  styles['align'] = 'center'
  styles['padding'] = 5
  -- text offset for buttons
  styles['press_offset'] = -1
  -- text drop shadow offset for labels
  styles['drop_shadow_offset'] = -2
end
function release_styles()
  local fmt = base.getmetatable(base.Font)
  for i, v in base.pairs(styles) do
    if base.type(v) == "userdata" then
      if fmt == base.getmetatable(v) then
        v:unload()
      end
    end
    styles[i] = nil
  end
end

-- style accessors
function set_style(name, value)
  styles[name] = value
end
function get_style(name)
  return styles[name]
end
-- returns an indexed table of styles
function get_styles(...)
  base.assert(styles, "GUI is not initialized")
  local ret = {}
  --for i = 1, base.select('#', ...) do
    --local arg = base.select(i, ...)
  local args = { ... }
  for i, arg in base.ipairs(args) do
    ret[arg] = styles[arg]
  end
  return ret
end
-- combines two style tables
function merge_styles(src, dest)
  for i, v in base.pairs(src) do
    dest[i] = v
  end
  return dest
end

-- color initialization
function init_colors()
  local Color = base.Color
  -- border around object
  colors['border'] = Color(245, 255, 113)
  colors['background'] = Color(61, 70, 40)
  colors['gradient'] = Color(245, 255, 113)
  -- color for object items like arrows and such
  colors['item'] = Color(104, 126, 58)
  -- font color
  colors['font'] = Color(245, 255, 113)
  -- highlight color
  colors['highlight'] = Color(245, 255, 113)
  -- highlighted text color
  colors['font_highlight'] = Color(0, 0, 0)
  -- dropshadow color for text
  colors['drop_shadow'] = Color(0, 0, 0)
end
function release_colors()
  for i, v in base.pairs(colors) do
    colors[i] = nil
  end
end

-- color accessors
function set_color(name, value)
  colors[name] = value
end
function get_color(name)
  return colors[name]
end
-- returns a table of colors
function get_colors(...)
  local ret = {}
  --local n = base.select('#', ...)
  --for i = 1, n do
    --local arg = base.select(i, ...)
  local args = { ... }
  for i, arg in base.ipairs(args) do
    -- make copies of colors
    if colors[arg] then
      ret[arg] = base.Color(colors[arg])
    end
  end
  return ret
end