-- performs a string search, returns a relative index (could be negative)
string.find_break = function(sz, start, reverse)
  local p = " %w"
  local dir = 1
  if reverse == true then
    sz = string.reverse(sz)
    start = string.len(sz) - start + 2
    p = "%w "
    dir = -1
  end
  local i = string.find(sz, p, start)
  if i == nil then
    i = string.len(sz) + 1
  end
  local i2 = string.find(sz, "\n", start)
  if i2 and i2 < i then
    i = i2
  end
  i = i - start + 1
  i = i * dir
  -- todo: returning 2 absolute indices is simpler
  return i
end