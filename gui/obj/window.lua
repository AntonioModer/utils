local base = _G
module("gui")

WindowG = {}
WindowGMT = { __index = WindowG }

base.setmetatable(WindowG, { __index = MContainerG })

WindowG.create = function(x, y, width, height, title, mt)
  local colors = get_colors('background', 'border', 'gradient', 'font')
  local styles = get_styles('font', 'padding')

  local self = MContainer(x, y, width, height, mt or WindowGMT)
  self.title = title

  self.colors = colors
  self.styles = merge_styles(self.styles, styles)

  return self
end

WindowG.destroy = function(self)
  self.title = nil
  self.colors = nil
  MContainerG.destroy(self)
end

WindowG.get_title_size = function(self)
  local pad = self.styles.padding
  local w = self.width - pad*2
  local h = self.styles.font:get_size() + pad*2
  return w, h
end

WindowG.test_point = function(self, lx, ly)
  local w, h = self:get_title_size()
  if lx < 0 or lx > self.width then
    return false
  end
  if ly < -h or ly > self.height then
    return false
  end
  return true
end

WindowG.redraw = function(self, delta)
  MContainerG.redraw(self, delta)

  local alpha = 1
  
  local c = self.sprite.canvas
  c:clear()

  -- draw background rectangle
  local titlew, titleh = 0, 0
  if self.title then
    titlew, titleh = self:get_title_size()
  end
  
  local l, t, r, b = 0, -self.height, self.width, titleh
  if self.colors.border then
    c:rect(l - 1, t - 1, r + 1, b + 1)
    c:set_fill_style(self.colors.border, alpha)
    c:fill()
  end
  if self.colors.background then
    c:rect(l, t, r, b)
    c:set_fill_style(self.colors.background, 1)
    c:fill()
  end
  if self.title then
    if self.colors.gradient then
      local grad = base.LinearGradient()
      grad:stop_point1(0, titleh, self.colors.gradient)
      grad:stop_point2(0, 0, self.colors.background)
      c:rect(l, 0, r, titleh)
      c:set_fill_style(grad, alpha)
      c:fill_gradient()
    end
    local pad = self.styles.padding
    c:move_to(pad, pad)
    c:set_font(self.styles.font, self.colors.font)
    local sz = self.styles.font:clamp(self.title, titlew, "...")
    c:write(sz)
  end
  -- todo clipping
end

WindowG.update = function(self, dt)
  MContainerG.update(self, dt)
  self:redraw()
end

Window = WindowG.create