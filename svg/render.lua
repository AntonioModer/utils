poly = require("utils.math.poly2")

local render = {}

render.style = {}

local curves = require("utils.math.curves")

-- fills a list of paths
function render.fill(paths)
  local alpha = render.style['fill-opacity'] or 1
  local color = render.style['fill'] or 0xffffff
  assert(alpha >= 0 and alpha <= 1)
  assert(color >= 0x0 and color <= 0xffffff)
  
  --[[
  for i = 1, #paths do
    local p = paths[i]
    s2.canvas:move_to(p[#p - 1], p[#p])
    for i = 1, #p, 2 do
      s2.canvas:line_to(p[i], p[i + 1])
    end
    s2.canvas:set_line_style(1, red, 1)
    s2.canvas:stroke()
  end
  ]]
  
  local polygons = poly.subpaths(paths)
  
  local s = {}
  --s.paths = {}
  --s.holes = {}
  local verts = {}
  while #polygons > 0 do
    local p = table.remove(polygons)
    --poly.cleanup(p, curves.epsilon)
    --poly.simplify(p, 0)
    if p.sub then
    --[[
      for i, h in ipairs(p.sub) do
        poly.cleanup(h, curves.epsilon)
        poly.simplify(p, 0)
      end
      ]]
      --terminal.print(#polygons, "holes", #p.sub)
      p = poly.cutholes(p, p.sub)
      --for i, v in ipairs(p.sub) do
      --  table.insert(s.holes, p.sub)
      --end
    end
    poly.triangulate(p, verts)
    --table.insert(s.paths, p)
  end

  local cols = array.uints(#verts/2)
  local alphas = array.floats(#verts/2)
  for i = 1, #verts/2 do
    cols[i] = color
    alphas[i] = alpha/4
  end
	s.type = 'triList'
  s.vertices = verts
  -- todo: how do we pass 8 digit hexes?
  s.colors = cols
  s.alpha = alphas
  return s
end

-- strokes a list of paths
function render.stroke(paths, color, alpha)
  -- unsupported
end

return render