local ctm = require("utils.svg.ctm")
local plot = require("utils.svg.plot")
local render = require("utils.svg.render")
local palette = require("utils.svg.colors")
local colors = require("utils.io.colors")

local insert = table.insert
local remove = table.remove

local style_types =
{
  ['fill'] = "color",
  ['fill-opacity'] = "number",
  ['stroke-width'] = "number",
  ['stroke'] = "color",
  ['stroke-opacity'] = "number"
}

-- ex: "#FFFF00", "red", "none"...
local function color(sz)
  if sz == nil then
    return
  end
  if sz == "none" then
    return
  end
  sz = string.lower(sz)
  if palette[sz] then
    local c = palette[sz]
    return colors.rgb_to_hex(c[1], c[2], c[3])
  end
  local rgb = "0x" .. string.gsub(sz, "#", "")
  local hex = tonumber(rgb)
  --local szrgb = string.format("0x%06x", hex)
  --assert(rgb == szrgb, rgb .. ' ' .. szrgb)
  return hex
end

-- ex: "10, 20 50, 60"
local function points(sz)
  local l = {}
  for a in string.gmatch(sz, "%s*([^,%s+]+)%s*") do
    insert(l, tonumber(a))
  end
  assert(#l > 0)
  assert(#l%2 == 0)
  return l
end

-- ex: "rotate(45)"
local function transform(sz)
  local l = {}
  for t, a in string.gmatch(sz, "(%w+)%s-%(([%s%w%.%,%+%-]+)%)") do
    insert(l, t)
    local args = {}
    for b in string.gmatch(a, "([+-]?[%deE.-]+)") do
      insert(args, tonumber(b))
    end
    insert(l, args)
  end
  return l
end

-- Parsing routines
local parse = {}

local ids = nil
local transforms = nil
local shapes = nil
local styles = nil

function parse.reset()
  ids = {}
  transforms = {}
  shapes = {}
  styles = {}
end

function parse.pre(x)
  -- the ‘id’ and ‘xml:base’ attributes are available on all SVG elements
  if x['id'] then
    ids[x.id] = x
  end
  if x['xlink:href'] then
  
  end
  
  -- apply transforms
  if x.transform then
    local q = transform(x.transform)
    assert(transforms[x] == nil)
    transforms[x] = #q/2

    while #q > 0 do
      local t = remove(q, 1)
      local a = remove(q, 1)
      ctm[t](unpack(a))
    end
  end

  styles[x] = {}
  -- flatten 'style' tag
  if x['style'] then
    for i, v in string.gmatch(x['style'], "%s*([^:;]+)%s*[:]%s*([^:;]+)") do
      styles[x][i] = v
    end
  end
  -- attributes
  for i, v in pairs(style_types) do
    if x[i] then
      styles[x][i] = x[i]
    end
  end
  -- set style
  for i, t in pairs(style_types) do
    local v = styles[x][i]
    if v then
      if t == "number" then
        v = tonumber(v)
      elseif t == "color" then
        v = color(v)
      end
      styles[x][i] = v
      render.style[i] = v
    end
  end
end

function parse.post(x)
  local paths = plot.pop_all()
  for _, path in ipairs(paths) do
    ctm.transformp(path)
  end
  
  if #paths > 0 then
    if styles[x]['fill'] or styles[x]['fill-opacity'] then
      local s = render.fill(paths)
      if s and #s.vertices > 0 then
        insert(shapes, s)
      end
    end
    --[[
    if styles[x]['stroke'] then
      local s = render.stroke(paths)
      if s and #s.vertices > 0 then
        insert(shapes, s)
      end
    end
    ]]
  end
  -- unset styles
  for i, v in pairs(styles[x]) do
    if x[i] then
      styles[i] = x[i]
      render.style[i] = nil
    end
  end
  styles[x] = nil
  
  if x.transform then
    for i = 1, transforms[x] do
      ctm.pop()
    end
    transforms[x] = nil
  end
end

-- Graphic elements
-- One of the element types that can cause graphics to be drawn onto the target canvas.
-- Specifically: ‘circle’, ‘ellipse’, ‘image’, ‘line’, ‘path’, ‘polygon’, ‘polyline’, ‘rect’, ‘text’ and ‘use’.
function parse.circle(x)
  local cx = tonumber(x.cx)
  local cy = tonumber(x.cy)
  local r = tonumber(x.r)
  plot.circle(cx, cy, r)
end

function parse.ellipse(x)
  local ex = tonumber(x.cx)
  local ey = tonumber(x.cy)
  local rx = tonumber(x.rx)
  local ry = tonumber(x.ry)
  plot.ellipse(ex, ey, rx, ry)
end

function parse.line(x)
  local x1 = tonumber(x.x1)
  local y1 = tonumber(x.y1)
  local x2 = tonumber(x.x2)
  local y2 = tonumber(x.y2)
  plot.line(x1, y1, x2, y2)
end

function parse.path(x)
  plot.begin_path()
  local args = {}  
  -- parse the drawing commands
  for c, a in string.gmatch(x.d, "([a-df-zA-DF-Z])([^a-df-zA-DF-Z]*)") do
    for arg in string.gmatch(a, "([+-]?[%deE.]+)") do
      insert(args, tonumber(arg))
    end
    plot.command(c, args)
    assert(#args == 0)
  end
  plot.end_path()
end

function parse.polygon(x)
  local p = points(x.points)
  plot.polygon(p)
end

function parse.polyline(x)
  local p = points(x.points)
  plot.polyline(p)
end

function parse.rect(x)
  local cx, cy = tonumber(x.x), tonumber(x.y)
  local w, h = tonumber(x.width), tonumber(x.height)
  local rx, ry = tonumber(x.rx), tonumber(x.ry)
  plot.rect(cx, cy, w, h, rx, ry)
end

-- unsupported
function parse.text(x)
end

function parse.use(x)
end

-- Container element
-- An element which can have graphics elements and other container elements as child elements
-- Specifically: ‘a’, ‘defs’, ‘glyph’, ‘g’, ‘marker’, ‘mask’, ‘missing-glyph’, ‘pattern’, ‘svg’, ‘switch’ and ‘symbol’
function parse.container(x)
  for i = 1, #x do
    local v = x[i]
    local t = v[0] -- xml.tag(v)
    if parse[t] then
      parse.pre(v)
      parse[t](v)
      parse.post(v)
    end
  end
end

function parse.a(x)
end

function parse.glyph(x)
end

function parse.marker(x)
end

function parse.mask(x)
end

parse['missing-glyph'] = function(x)
end

function parse.pattern(x)
end

function parse.switch(x)
end

-- Structural elements
-- The structural elements are those which define the primary structure of an SVG document
function parse.defs(x)
end

function parse.g(x)
  parse.container(x)
end

function parse.svg(x)
  parse.container(x)
end

function parse.symbol(x)  
  parse.container(x)
end

function parse.use(x)
end

-- css
function parse.style(x)
  local sz = x[1]
  -- todo parse style classes
  for c, v in string.gmatch(sz, ".([%w_]+)%s-{(.+)}%s-\n") do
    --terminal.log(c .. ' ' .. v)
  end
end

function parse.xml(x)
  --local s = xml.find(x, "svg")

  local s
  for i, v in ipairs(x) do
    if v[0] == "svg" then
      s = v
      break
    end
  end

  if s == nil then
    error("svg tag not found")
    return
  end
  --assert(parse.version == "1.1")
  
  parse.reset()
  ctm.reset()
  
  parse.svg(s)
  
  return shapes
end

--require("LuaXml")
require("utils.table.xml")

function parse.string(sz)
  local x = table.fromxml(sz)
  --local x = xml.eval(sz)
  if x then
    return parse.xml(x)
  end
end

function parse.file(fn)
  --local x = xml.load(fn)
  local f = io.open(fn)
  assert(f, "could not open file: " .. fn)
  local sz = f:read("*a")
  return parse.string(sz)
end

svg = {}

svg.eval = parse.string
svg.load = parse.file