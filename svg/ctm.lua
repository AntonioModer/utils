local insert = table.insert
local remove = table.remove

local tforms = {}

local ctm = {}

ctm.reset = function()
  tforms = {}
end

-- identity 3x2 matrix:
-- 1 0 0
-- 0 1 0
ctm.translate = function(tx, ty)
  assert(tx)
  ty = ty or 0
  insert(tforms, { 1, 0, 0, 1, tx, ty })
end

-- a c d  sx 0 0
-- b d e  0 sy 0
ctm.scale = function(sx, sy)
  assert(sx)
  sy = sy or 1
  insert(tforms, { sx, 0, 0, sy, 0, 0 })
end

-- a c d  cos(a) -sin(a) 0
-- b d e  sin(a) cos(a) 0
ctm.rotate = function(a, x, y)
  assert(a)
  --assert(x == nil and y == nil, "rotate around point unsupported")
  x = x or 0
  y = y or 0
  a = math.rad(a)
  local c = math.cos(a)
  local s = math.sin(a)
  insert(tforms, { c, s, -s, c, tx, ty })
  --insert(tforms, { c, s, -s, c, 0, 0 })
end

-- a c d  1 tan(sx) 0
-- b d e  0 1 0
ctm.skewX = function(sx)
  assert(sx)
  -- convert to radians
  sx = math.rad(sx)
  local t = math.tan(sx)
  insert(tforms, { 1, 0, t, 1, 0, 0 })
end

-- a c d  1 0 0
-- b d e  tan(sy) 1 0
ctm.skewY = function(sy)
  assert(sy)
  -- convert to radians
  sy = math.rad(sy)
  local t = math.tan(sy)
  insert(tforms, { 1, t, 0, 1, 0, 0 })
end

ctm.matrix = function(...)
  local m = { ... }
  assert(#m == 6, "matrix must have 6 parameters")
  insert(tforms, m)
end

ctm.pop = function()
  remove(tforms)
end

-- |a c tx|   |x|   |x'|
-- |b d ty| x |y| = |y'|

-- x' = ax + cy + tx
-- y' = bx + dy + ty
ctm.transform = function(x, y)
  if #tforms == 0 then
    return x, y
  end
  local lx, ly = x, y
  for i = #tforms, 1, -1 do
    local m = tforms[i]
    local tx = lx*m[1] + ly*m[3] + m[5]
    local ty = lx*m[2] + ly*m[4] + m[6]
    lx, ly = tx, ty
  end
  return lx, ly
end

ctm.transformp = function(p)
  for i = 1, #p, 2 do
    p[i], p[i + 1] = ctm.transform(p[i], p[i + 1])
  end
  return p
end

return ctm