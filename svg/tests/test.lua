display:create("assets", 1000, 600, 32, true, 30, false, 4)

terminal = require('utils.log.terminal')
profile = require("utils.log.profile")
profile.hookall("Lua")
require('utils.agen.display')
require('utils.svg.parse')
require('utils.io.lfs')
local curves = require("utils.math.curves")

local orange = Color(255, 165, 0)

camera = Camera()
scene = Layer()
s2 = Sprite()
s2:set_scale(1, -1)
scene:add_child(s2)
scene:add_child(camera)
display.viewport.camera = camera

local files = lfs.get_rdir("utils/svg/tests/data")
local i = 10

function keyboard:on_press(k)
  if k == KEY_UP then
    curves.epsilon = curves.epsilon*2
  elseif k == KEY_DOWN then
    curves.epsilon = curves.epsilon/2
  elseif k == KEY_LEFT then
    i = (i - 1)
    if i == 0 then i = #files end
  elseif k == KEY_RIGHT then
    i = (i + 1)
    if i > #files then i = 1 end
  end
  
  s2.canvas:clear()
  s2.canvas:square(8000)
  s2.canvas:set_fill_style(orange, 1)
  s2.canvas:fill()
  
  local shapes = svg.load(files[i])
  local nverts = 0
  for i, s in ipairs(shapes) do
    s2.canvas:add_shape(s)
    nverts = nverts + #s.vertices
  end

  profile.stop()
  
  terminal.clear()
  terminal.printf("%d. %s (nshapes:%d, nverts:%d, curveres:%f)", i, files[i], #shapes, nverts, curves.epsilon)

  local n = 1
  for f, c, t, d in profile.query("time", 20) do
    terminal.printf("%d.'%s'x%d time:%s (%s)", n, f, c, t, d)
    n = n + 1
  end

  profile.reset()
end

-- allow zooming the camera via the wheel
function mouse:on_wheelmove(z)
  if ( z < 0 ) then
    z = 1.1
  else
    z = 0.9
  end
  camera:change_scale(z, z)
end

-- allow moving the camera via the mouse
function mouse:on_move(x, y)
  if (mouse:is_down(MBUTTON_LEFT)) then
    local r = -math.rad ( camera.rotation )

    local cos_d = math.cos ( r )
    local sin_d = math.sin ( r )

    local tx = cos_d * x - sin_d * y
    local ty = cos_d * y + sin_d * x

    tx = tx * camera.scalex
    ty = ty * camera.scaley
    camera:change_position(-tx, -ty)
  end

  if (mouse:is_down(MBUTTON_RIGHT)) then
    camera.rotation = camera.rotation + y / 2 + x / 2
  end
end

keyboard.on_press()