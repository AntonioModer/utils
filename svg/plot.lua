curves = require("utils.math.curves")
require("utils.table.util")

local remove = table.remove
local insert = table.insert

local open = false
local pnx, pny = 0, 0
local cpx, cpy = 0, 0
local spx, spy = 0, 0
local path = {}
local paths = {}

-- plots paths
local plot = {}

-- push a new path
plot.push = function(p)
  assert(#p > 0)
  insert(paths, p)
end

-- pop all paths
plot.pop_all = function()
  assert(#path == 0)
  local p = paths
  paths = {}
  return p
end

-- begin a new path
plot.begin_path = function()
  open = false
  pnx, pny = 0, 0
  cpx, cpy = 0, 0
  spx, spy = 0, 0
  path = {}
end

-- end the last path
plot.end_path = function()
  if #path > 0 then
    plot.push(path)
    open = false
    path = {}
  end
  open = false
  pnx, pny = 0, 0
  cpx, cpy = 0, 0
  spx, spy = 0, 0
end

plot.move = function(x, y)
  assert(x and y)
  pnx, pny = x, y
  cpx, cpy = x, y
  spx, spy = x, y
  if #path > 0 then
    plot.push(path)
    open = false
    path = {}
  end
end

plot.plot = function(x, y)
  assert(x and y)
  if pnx == x and pny == y then
    return
  end
  -- path start
  if open == false then
    open = true
    spx, spy = pnx, pny
    -- new vertex
    insert(path, spx)
    insert(path, spy)
  end
  
  -- pen
  pnx, pny = x, y
  cpx, cpy = x, y
  
  -- new vertex
  insert(path, x)
  insert(path, y)
end

plot.close = function()
  if open == true then
    plot.L(spx, spy)
    plot.push(path)
    open = false
    path = {}
  end
end

-- close path
plot.z = function()
  plot.close()
end
plot.Z = function()
  plot.z()
end

-- move to
plot.M = function(x, y)
  plot.move(x, y)
end
plot.m = function(rx, ry)
  rx, ry = rx + pnx, ry + pny
  plot.M(rx, ry)
end

plot.L = function(x, y, ...)
  plot.plot(x, y)
end
plot.l = function(rx, ry)
  rx, ry = rx + pnx, ry + pny
  plot.L(rx, ry)
end

plot.H = function(h)
  plot.L(h, pny)
end
plot.h = function(h)
  plot.l(h, 0)
end

plot.V = function(v)
  plot.L(pnx, v)
end
plot.v = function(v)
  plot.l(0, v)
end

--- arc
-- (rx ry x-axis-rotation large-arc-flag sweep-flag x y)
-- not very elegant but compliant with svg
plot.A = function(rx, ry, xar, laf, swf, x2, y2)
  -- If the endpoints are identical, omit the arc entirely
  if pnx == x2 and pny == y2 then
    assert(false)
    return
  end

  -- rotation
  xar = math.rad(xar%360)

  -- midpoint
  local x1, y1 = pnx, pny

  -- Correction of out-of-range radii
  
  -- Step 1: Ensure radii are non-zero
  if rx == 0 or ry == 0 then
    plot.L(x, y)
    return
  end
  
  -- Step 2: Ensure radii are positive
  rx = math.abs(rx)
  ry = math.abs(ry)

  -- Step 3: Ensure radii are large enough
  local ca = math.cos(xar)
  local sa = math.sin(xar)
  local dx = (x1 - x2)/2
  local dy = (y1 - y2)/2
  local x1t = ca*dx + sa*dy
  local y1t = -sa*dx + ca*dy
  local x1t2 = x1t*x1t
  local y1t2 = y1t*y1t
  local rx2 = rx*rx
  local ry2 = ry*ry
  
  local v = x1t2/rx2 + y1t2/ry2
  if v > 1 then
    rx = math.sqrt(v)*rx
    ry = math.sqrt(v)*ry
    assert(rx > 0 and ry > 0)
    rx2 = rx*rx
    ry2 = ry*ry
  end
  
  -- Conversion from endpoint to center
  
  -- Step 2: Compute (cx′, cy′)
  local v1 = rx2*ry2 - rx2*y1t2 - ry2*x1t2
  local v2 = rx2*y1t2 + ry2*x1t2
  assert(v2 ~= 0)
  local v = math.sqrt(v1/v2)
  assert(v >= 0)
  if laf == swf then
    v = -v
  end
  local cxt = v*(rx*y1t/ry)
  local cyt = v*-(ry*x1t/rx)
  
  -- Step 3: Compute (cx, cy) from (cx′, cy′)
  local mx = (x2 + x1)/2
  local my = (y2 + y1)/2
  local cx = (ca*cxt - sa*cyt) + mx
  local cy = (sa*cxt + ca*cyt) + my
  
  -- Step 4: Compute initial angle and arc
  local ux = (x1t - cxt)/rx
  local uy = (y1t - cyt)/ry
  local vx = (-x1t - cxt)/rx
  local vy = (-y1t - cyt)/ry
  local u2 = ux*ux + uy*uy
  local v2 = vx*vx + vy*vy
  local n = math.sqrt(u2)
  assert(n > 0)
  local p = ux
  local ia = math.acos(p/n)
  if uy < 0 then
    ia = -ia
  end
  n = math.sqrt(u2*v2)
  p = ux*vx + uy*vy
  assert(n > 0)
  local ea = math.acos(p/n)
  if ux*vy - uy*vx < 0 then
    ea = -ea
  end
  if swf == 0 and ea > 0 then
    ea = ea - math.pi*2
  elseif swf ~= 0 and ea < 0 then
    ea = ea + math.pi*2
  end
  --ia = ia%(math.pi*2)
  --ea = ea%(math.pi*2)

  local p = curves.earc(rx, ry, cx, cy, ia, ea + ia, xar)
  for i = 1, #p, 2 do
    plot.L(p[i], p[i + 1])
  end
  plot.L(x2, y2)
end
plot.a = function(rx, ry, xar, laf, swf, x, y)
  x, y = x + pnx, y + pny
  plot.A(rx, ry, xar, laf, swf, x, y)
end

-- Quadratic bezier
plot.Q = function(x2, y2, x3, y3)
  local x1, y1 = pnx, pny
  local p = curves.qbezier(x1, y1, x2, y2, x3, y3)
  for i = 1, #p, 2 do
    plot.L(p[i], p[i + 1])
  end
  -- store the control point
  cpx, cpy = x2, y2
end
plot.q = function(x2, y2, x3, y3)
  x2, y2 = x2 + pnx, y2 + pny
  x3, y3 = x3 + pnx, y3 + pny
  plot.Q(x2, y2, x3, y3)
end

plot.T = function(x3, y3)
  -- use last control point
  local x1, y1 = pnx, pny
  local x2, y2 = x1, y1
  -- mirror the last control point
  local dx, dy = cpx - x1, cpy - y1
  x2, y2 = x1 - dx, y1 - dy

  plot.Q(x2, y2, x3, y3)
end
plot.t = function(x3, y3)
  x3, y3 = x3 + pnx, y3 + pny
  plot.T(x3, y3)
end

-- Cubic Bezier
plot.C = function(x2, y2, x3, y3, x4, y4)
  local x1, y1 = pnx, pny
  local p = curves.cbezier(x1, y1, x2, y2, x3, y3, x4, y4)
  for i = 1, #p, 2 do
    plot.L(p[i], p[i + 1])
  end
  -- store the second control point
  cpx, cpy = x3, y3
end
plot.c = function(x2, y2, x3, y3, x4, y4)
  x2, y2 = x2 + pnx, y2 + pny
  x3, y3 = x3 + pnx, y3 + pny
  x4, y4 = x4 + pnx, y4 + pny
  plot.C(x2, y2, x3, y3, x4, y4)
end

plot.S = function(x3, y3, x4, y4)
  -- use last control point
  local x1, y1 = pnx, pny
  local x2, y2 = x1, y1

  -- mirror the last control point
  local dx, dy = cpx - x1, cpy - y1
  x2, y2 = x1 - dx, y1 - dy
  
  plot.C(x2, y2, x3, y3, x4, y4)
end
plot.s = function(x3, y3, x4, y4)
  x3, y3 = x3 + pnx, y3 + pny
  x4, y4 = x4 + pnx, y4 + pny
  plot.S(x3, y3, x4, y4)
end

-- arguments per command
local path_nargs =
{
  z = 0, m = 2, l = 2, h = 1, v = 1, a = 7, q = 4, t = 2, c = 6, s = 4,
  Z = 0, M = 2, L = 2, H = 1, V = 1, A = 7, Q = 4, T = 2, C = 6, S = 4
}

local t = {}
plot.command = function(c, q)
  repeat
    local n = path_nargs[c]
    for i = 1, n do
      --assert(type(q[1] == "number"))
      insert(t, remove(q, 1))
    end
    plot[c](unpack(t))
    table.clear(t)
    -- if a moveto is followed by multiple pairs of coordinates
    -- the subsequent pairs are treated as implicit lineto commands
    if c == "M" or c == "m" then
      if c == "M" then
        c = "L"
      else
        c = "l"
      end
    end
  until #q == 0
end

-- shapes
plot.polyline = function(p)
  assert(#path == 0 and open == false)
  assert(#p >= 4)
  --plot.push(paths, p)
  plot.M(p[1], p[2])
  for i = 3, #path, 2 do
    plot.L(p[i], p[i + 1])
  end
  plot.end_path()
end

plot.polygon = function(p)
  plot.M(p[1], p[2])
  for i = 3, #path, 2 do
    plot.L(p[i], p[i + 1])
  end
  plot.z()
  plot.end_path()
end

plot.line = function(x, y, x2, y2)
  local l = { x, y, x2, y2 }
  plot.polyline(l)
end

plot.rect = function(x, y, w, h, rx, ry)
  assert(w > 0 and h > 0)
  local l, t = x, y
  local r, b = l + w, t + h
  local p
  if (rx == nil or ry == nil) or (rx == 0 and ry == 0) then
    -- not rounded?
    p = { l,t, r,t, r,b, l,b }
  else
    -- rounded
    p = curves.raabb(l, t, r, b, rx, ry)
  end
  plot.polygon(p)
end

plot.ellipse = function(x, y, rx, ry)
  local p = curves.ellipse(rx, ry, x, y)
  plot.polygon(p)
end

plot.circle = function(x, y, r)
  local p = curves.circle(r, x, y)
  plot.polygon(p)
end

return plot