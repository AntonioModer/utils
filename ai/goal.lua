local base = _G
module("ai")

GoalG = {}
GoalGMT = { __index = GoalG }

GoalG.create = function(mt)
  local goal = {}
  base.setmetatable(goal, mt or GoalGMT)

  return goal
end

GoalG.destroy = function(self)
	
end

GoalG.reset = function(self)

end

GoalG.process = function(self, dt, agent)
  return inactive, 0
end

GoalG.message = function(self, agent, msg, ...)
  return false
end

Goal = GoalG.create

-- XML
GoalG.getXML = function(self)
  local g = base.xml.new("Goal")
  g.type = self:getType()
  for i, v in base.pairs(self) do
    g[i] = v
  end
  return g
end

GoalG.createXML = function(goal)
  local c = base.ai[goal.type .. "G"]
  return c.createXML(goal)
  --local c = base.ai[goal.type]
  --return c(goal)
end