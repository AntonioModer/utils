local base = _G
module("ai")

local TweenG = {}
local TweenGMT = { __index = TweenG }

base.setmetatable(TweenG, { __index = Goal })

TweenG.create = function(property, value, time, elapsed)
  local goal = Goal(TweenGMT)

  goal.property = property
  goal.value = value
  goal.time = time
  goal.elapsed = elapsed or 0

  return goal
end

TweenG.destroy = function(self)
  self.property = nil
  self.value = nil
  self.time = nil
  self.elapsed = nil
  GoalG.destroy(self)
end

TweenG.reset = function(self)
  self.elapsed = 0
end

TweenG.process = function(self, dt, agent)
  self.elapsed = self.elapsed + dt
  local scenenode = getComponent(agent, "scenenode")

  -- condition
  if self.elapsed >= self.time then
    scenenode[self.property] = self.value
    local used = dt -(self.elapsed - self.time)
    self:reset()
    return 'completed', used
  end

  -- output
  local ratio = dt/(self.time - (self.elapsed - dt))
  local value = scenenode[self.property]
  scenenode[self.property] = value + (self.value - value)*ratio

  return 'active', dt
end

Tween = TweenG.create


-- XML
TweenG.getType = function(self)
  return "Tween"
end

TweenG.createXML = function(g)
  local value = base.tonumber(g.value)
  local time = base.tonumber(g.time)
  local elapsed = base.tonumber(g.elapsed)
  return TweenG.create(g.property, value, time, elapsed)
end
