local base = _G
module("ai")

local ModulateG = {}
local ModulateGMT = { __index = ModulateG }

base.setmetatable(ModulateG, { __index = GoalG })

ModulateG.create = function(color, time, elapsed)
  local goal = Goal(ModulateGMT)

  goal.color = color
  goal.time = time
  goal.elapsed = elapsed or 0

  return goal
end

ModulateG.destroy = function(self)
  self.color = nil
  self.time = nil
  self.elapsed = nil
  GoalG.destroy(self)
end

ModulateG.reset = function(self)
  self.elapsed = 0
end

ModulateG.process = function(self, dt, agent)
  self.elapsed = self.elapsed + dt
  local scenenode = getComponent(agent, "scenenode")

  -- condition
  if self.elapsed >= self.time then
    scenenode.color = self.color
    local used = dt - (self.elapsed - self.time)
    self.elapsed = 0
    return 'completed', used
  end

  -- output
  local c = scenenode.color
  local ratio = dt/(self.time - (self.elapsed - dt))
  c.red = c.red +(self.color.red - c.red)*ratio
  c.green = c.green +(self.color.green - c.green)*ratio
  c.blue = c.blue +(self.color.blue - c.blue)*ratio

  return 'active', dt
end

Modulate = ModulateG.create



-- XML
ModulateG.getType = function(self)
  return "Modulate"
end

ModulateG.createXML = function(g)
  local color = base.tonumber(g.color)
  local time = base.tonumber(g.time)
  local elapsed = base.tonumber(g.elapsed)
  return ModulateG.create(color, time, elapsed)
end
