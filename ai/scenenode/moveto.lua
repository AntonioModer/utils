local base = _G
module("ai")

local MoveToG = {}
local MoveToGMT = { __index = MoveToG }

base.setmetatable(MoveToG, { __index = Goal })

MoveToG.create = function(valuex, valuey, speed, elapsed)
  local goal = Goal(MoveToGMT)

  goal.valuex = valuex
  goal.valuey = valuey
  goal.speed = speed
  goal.elapsed = elapsed or 0

  return goal
end

MoveToG.destroy = function(self)
  goal.valuex = nil
  goal.valuey = nil
  self.speed = nil
  self.elapsed = nil
  GoalG.destroy(self)
end

MoveToG.reset = function(self)
  self.elapsed = 0
end

MoveToG.process = function(self, dt, agent)
  self.elapsed = self.elapsed + dt
  local scenenode = getComponent(agent, "scenenode")

  -- teleport
  if self.speed == nil or self.speed == 0 then
    scenenode.x = self.valuex
    scenenode.y = self.valuey
    self:reset()
    return 'completed', 0
  end

  -- condition
  local dx = self.valuex - scenenode.x
  local dy = self.valuey - scenenode.y
  local dist = base.math.sqrt(dx*dx + dy*dy)
  local eta = dist/self.speed
  if eta < dt then
    scenenode.x = self.valuex
    scenenode.y = self.valuey
    self:reset()
    return 'completed', eta
  end

  -- output
  local step = self.speed*dt
  dx, dy = dx/dist*step, dy/dist*step
  scenenode.x = scenenode.x + dx
  scenenode.y = scenenode.y + dy

  return 'active', dt
end

MoveTo = MoveToG.create


-- XML
MoveToG.getType = function(self)
  return "Tween"
end

MoveToG.createXML = function(g)
  local valuex = base.tonumber(g.valuex)
  local valuey = base.tonumber(g.valuey)
  local speed = base.tonumber(g.speed)
  local elapsed = base.tonumber(g.elapsed)
  return MoveToG.create(g.propertyx, g.propertyy, valuex, valuey, speed, elapsed)
end
