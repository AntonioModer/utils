local base = _G
local table = table
module("ai")

SequenceG = {}
SequenceGMT = { __index = SequenceG }

base.setmetatable(SequenceG, { __index = GoalG })

SequenceG.create = function(loop)
  local goal = Goal(mt or SequenceGMT)

  goal.goals = {}
  goal.loop = loop

  return goal
end

SequenceG.destroy = function(self)
  self:clear()

  self.goals = nil
  self.loop = nil
  
  GoalG.destroy(self)
end

SequenceG.reset = function(self)
  if self.active ~= nil then
    self.goals[self.active]:reset()
    self.active = nil
  end
  self.first = nil
end

SequenceG.process = function(self, dt, agent)
  if #self.goals == 0 then
    return inactive, 0
  end

  -- activate if inactive
  if self.active == nil then
    self.active = 1
  end

  -- process the active goal
  local status, used = self.goals[self.active]:process(dt, agent)

  -- the active goal has destroyed this sequence
  if self.active == nil then
    return 'completed', used
  end

  if status == 'active' then
    self.first = nil
    return 'active', used
  elseif status == 'failed' then
    -- the sequence has failed
    self.active = nil
    self.first = nil
    return 'failed', used
  elseif status == 'completed' then
    -- remember the first goal that completed immediately
    if used == 0 then
      self.first = self.first or self.active
    else
      self.first = nil
    end

    -- move to the next goal
    self.active = self.active + 1
    if self.active > #self.goals then
      if self.loop ~= true then
        -- the sequence is complete
        self.active = nil
        self.first = nil
        return 'completed', used
      end
      self.active = 1
    end

    -- all goals have completed immediately in a loop
    if self.loop == true then
      if self.first == self.active and used == 0 then
        return 'active', used
      end
    end

    if dt == used then
      self.first = nil
      return 'active', used
    end

    return self:process(dt - used, agent)
  end
end

SequenceG.push = function(self, goal)
  base.assert(goal)
  table.insert(self.goals, goal)
end

SequenceG.clear = function(self)
  self:reset()
  while #self.goals > 0 do
    local last = table.remove(self.goals)
    last:Destroy()
  end
end

SequenceG.message = function(self, agent, msg, ...)
  if self.active then
    return self.goals[self.active]:message(agent, msg, ...)
  end
  return false
end

Sequence = SequenceG.create



-- XML
SequenceG.getType = function(self)
  return "Sequence"
end

SequenceG.getXML = function(self)
  local g = base.xml.new("Goal")
  g.type = self:getType()
  g.active = self.active
  g.loop = self.loop
  for i, v in base.ipairs(self.goals) do
    local sub = v:getXML()
    g:append(sub)
  end
  return g
end

SequenceG.createXML = function (goal)
  local loop = base.toboolean(goal.loop)
  local g = SequenceG.create(loop)
  g.active = base.tonumber(goal.active)
  for i = 1, #goal, 1 do
    local sub = GoalG.createXML(goal[i])
    table.insert(g.goals, sub)
  end
  return g
end