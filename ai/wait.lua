local base = _G
module("ai")

local WaitG = {}
local WaitGMT = { __index = WaitG }

base.setmetatable(WaitG, { __index = GoalG })

WaitG.create = function(time, elapsed)
  local goal = Goal(WaitGMT)

  goal.time = time
  goal.elapsed = elapsed or 0

  return goal
end

WaitG.destroy = function(self)
  self.time = nil
  self.elapsed = nil
  GoalG.destroy(self)
end

WaitG.reset = function(self)
  self.elapsed = 0
end

WaitG.process = function(self, dt, agent)
  self.elapsed = self.elapsed + dt

  -- is the waiting period over?
  if self.elapsed >= self.time then
  local used = dt - (self.elapsed - self.time)
    self:reset()
    return 'completed', used
  end
  return 'active', dt
end

Wait = WaitG.create

-- XML
WaitG.getType = function(self)
  return "Wait"
end

WaitG.createXML = function(g)
  local time = base.tonumber(g.time)
  local elapsed = base.tonumber(g.elapsed)
  return WaitG.create(time, elapsed)
end
