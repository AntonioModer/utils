local base = _G
module("ai")

SendMSGG = {}
SendMSGGMT = { __index = SendMSGG }

base.setmetatable(SendMSGG, { __index = GoalG })

SendMSGG.create = function(receiver, msg, mt)
  local goal = Goal(mt or SendMSGGMT)

  goal.receiver = receiver
  goal.msg = msg

  return goal
end

SendMSGG.destroy = function(self)
  self.receiver = nil
  self.msg = nil
  GoalG.destroy(self)
end

SendMSGG.process = function(self, dt, agent)
  local goal = goals[self.receiver]
  if goal then
    goal:message(self.receiver, self.msg)
  end
  return 'completed', 0
end

SendMSG = SendMSGG.create

-- XML
SendMSGG.getType = function(self)
  return "SendMSG"
end

SendMSGG.createXML = function(g)
  local receiver = base.tonumber(g.receiver)
  return SendMSGG.create(receiver, g.msg)
end