local base = _G
module("ai")

local CompleteG = {}
local CompleteGMT = { __index = CompleteG }

base.setmetatable(CompleteG, { __index = GoalG })

CompleteG.create = function()
  return Goal(CompleteGMT)
end

CompleteG.process = function(self, dt, agent)
  return 'completed', 0
end

Complete = CompleteG.create


-- XML
CompleteG.getType = function(self)
  return "Complete"
end