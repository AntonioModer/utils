local base = _G
module("ai")

local FailG = {}
local FailGMT = { __index = FailG }

base.setmetatable(FailG, { __index = GoalG })

FailG.create = function()
  return Goal(FailGMT)
end

FailG.process = function(self, dt, agent)
  return 'failed', 0
end

Fail = FailG.create


-- XML
FailG.getType = function(self)
  return "Fail"
end