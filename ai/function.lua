local base = _G
module("ai")

local FunctionG = {}
local FunctionGMT = { __index = FunctionG }

base.setmetatable(FunctionG, { __index = GoalG })

FunctionG.create = function(func, ...)
  local goal = Goal(FunctionGMT)

  goal.func = func
  goal.params = { ... }

  return goal
end

FunctionG.destroy = function(self)
  self.func = nil
  self.params = nil
  GoalG.destroy(self)
end

FunctionG.process = function(self, dt, agent)
  base[self.func](base.unpack(self.params))
  --self.func(base.unpack(self.params))
  return 'completed', 0
end

Function = FunctionG.create

-- XML
FunctionG.getType = function(self)
  return "Function"
end