local base = _G
module("ai")

ReceiveMSGG = {}
ReceiveMSGGMT = { __index = ReceiveMSGG }

base.setmetatable(ReceiveMSGG, { __index = GoalG })

ReceiveMSGG.create = function(msg, mt)
  local goal = Goal(mt or ReceiveMSGGMT)

  goal.msg = msg
  goal.received = false

  return goal
end

ReceiveMSGG.destroy = function(self)
  self.msg = nil
  self.received = nil
  GoalG.destroy(self)
end

ReceiveMSGG.reset = function(self)
  self.received = false
end

ReceiveMSGG.process = function(self, dt, agent)
  if self.received == true then
    self.received = false
    return 'completed', 0
  end
  return 'active', dt
end

ReceiveMSGG.message = function(self, agent, msg, ...)
  if self.msg == msg and self.received == false then
    self.received = true
    return true
  end
  return false
end

ReceiveMSG = ReceiveMSGG.create


-- XML
ReceiveMSGG.getType = function(self)
  return "ReceiveMSG"
end

ReceiveMSGG.createXML = function(g)
  return ReceiveMSGG.create(g.msg)
end