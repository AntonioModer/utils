-- base class
require("utils.ai.goal")

-- conditionals
require("utils.ai.fail")
require("utils.ai.complete")
require("utils.ai.wait")

-- composite goals
require("utils.ai.sequence")
require("utils.ai.parallel")
require("utils.ai.selector")

-- callbacks
require("utils.ai.function")
require("utils.ai.sendmsg")
require("utils.ai.receivemsg")

-- tweeners (visual)
require("utils.ai.scenenode.tween")
require("utils.ai.scenenode.moveto")
require("utils.ai.scenenode.modulate")

local base = _G
local pairs = pairs
module("ai")

agents = {}
goals = {}
components = {}

function addAgent(id, agent)
  agents[id] = agent
end

function removeAgent(id)
  agents[id] = nil
end

function getComponent(id, component)
  base.assert(components[component], "Invalid component")
  return components[component][id]
end

function setComponent(id, component, value)
  if components[component] == nil then
    components[component] = {}
  end
  components[component][id] = value
end

function message(id, ...)
  goals[id]:message(agents[id], ...)
end

function setGoal(id, goal)
  base.assert(id, "Agent id is nil")
  base.assert(goal, "Agent goal is nil")
  goals[id] = goal
end

function updateGoals(dt)
  for id, goal in pairs(goals) do
    goal:process(dt, id)
  end
end