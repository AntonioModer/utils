local base = _G
local math = math
local table = table
module("ai")

ParallelG = {}
ParallelGMT = { __index = ParallelG }

base.setmetatable(ParallelG, { __index = GoalG })

ParallelG.create = function(loop, mt)
  local goal = Goal(mt or ParallelGMT)

  goal.goals = {}
  goal.loop = loop

  return goal
end

ParallelG.destroy = function(self)
  self:clear()

  self.goals = nil
  self.loop = nil
  
  GoalG.destroy(self)
end

ParallelG.reset = function(self)
  if self.active ~= nil then
    for i, v in base.pairs(self.active) do
      self.goals[v]:reset()
    end
    self.active = nil
  end
end

ParallelG.process = function(self, dt, agent)
  if #self.goals == 0 then
    return 'inactive', 0
  end

  -- activate if inactive
  if self.active == nil then
    self.active = {}
    for i in base.pairs(self.goals) do
      table.insert(self.active, i)
    end
  end

  local maxUsed = 0
  for i, v in base.pairs(self.active) do
    local active = self.goals[v]

    -- process the current goal
    local status, used = active:process(dt, agent)

    -- keep processing the current goal if the parallel is looped
    if self.loop == true then
      local used2 = used
      while status == completed and used2 > 0 do
        status, used2 = active:process(dt - used, agent)
        used = used + used2
      end
    end
    maxUsed = math.max(maxUsed, used)

    -- the active goal has destroyed this parallel
    if self.active == nil then
      return 'completed', maxUsed
    end

    if status == 'failed' then
      -- the failed subgoal is no longer active
      table.remove(self.active, i)
      -- reset all other active subgoals
      self:reset()
      return 'failed', maxUsed
    elseif status == 'completed' then
      if self.loop ~= true then
        table.remove(self.active, i)
      end
    end
  end

  if #self.active == 0 then
    self.acitve = nil
    return 'completed', maxUsed
  end

  return 'active', maxUsed
end

ParallelG.message = function(self, agent, msg, ...)
  local result = false
  if self.active then
    for i, v in base.pairs(self.active) do
      local active = self.goals[v]
      if active:message(agent, msg, ...) == true then
        result = true
        break
      end
    end
  end
  return result
end

ParallelG.push = function(self, goal)
  base.assert(goal)
  table.insert(self.goals, goal)
end

ParallelG.clear = function(self)
  self:reset()
  while #self.goals > 0 do
    local last = table.remove(self.goals)
    last:Destroy()
  end
end

Parallel = ParallelG.create




-- XML
ParallelG.getType = function(self)
  return "Parallel"
end

ParallelG.createXML = function (goal)
  local loop = base.toboolean(goal.loop)
  local g = ParallelG.create(loop)
  --g.active = base.tonumber(goal.active)
  for i = 1, #goal, 1 do
    local sub = GoalG.createXML(goal[i])
    table.insert(g.goals, sub)
  end
  return g
end

ParallelG.getXML = function(self)
  local g = base.xml.new("Goal")
  g.type = self:getType()
  g.loop = self.loop
  if self.active then
    local active = ""
    for i, v in ipairs(self.active) do
      active = active .. v
      if i < #self.active then
        active = active .. ", "
      end
    end
    g.active = active
  end
  for i, v in base.ipairs(self.goals) do
    local sub = v:getXML()
    g:append(sub)
  end
  return g
end