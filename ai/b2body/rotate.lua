local base = _G
module ( "ai" )

RotateG = {}
RotateGMT = { __index = RotateG }

base.setmetatable ( RotateG, { __index = Goal } )

RotateG.create = function ( angle, turnRate )
  local goal = {}
  base.setmetatable ( goal, RotateGMT )

  goal.angle = angle
  goal.turnRate = turnRate

  return goal
end

RotateG.destroy = function ( self )
  self.angle = nil
  self.turnRate = nil
end

RotateG.process = function ( self, dt, agent )
  local angle = self.angle
  -- teleport
  if self.turnRate == nil or self.turnRate == 0 then
    agent:SetAngle ( angle )
    return completed, 0
  end

  local currentAngle = agent.def.angle
  local desiredAV = self.turnRate
  if currentAngle > angle then
    desiredAV = -desiredAV
  end
  -- condition
  local eta = agent:GetTurnTime ( angle, desiredAV )
  if eta < dt then
    agent:SetAngle ( angle )
    return completed, eta
  end
  -- output
  local step = desiredAV * dt
  agent:SetAngle ( currentAngle + step )
  -- we are rotating the body manually so clear all forces acting upon it
  agent.b2body:SetAngularVelocity ( 0 )

  return active, dt
end

Rotate = RotateG.create

-- XML
RotateG.getType = function ( self )
  return "Rotate"
end

RotateG.createXML = function ( g )
  return RotateG.create ( g.angle, g.turnRate )
end
