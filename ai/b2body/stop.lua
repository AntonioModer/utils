local base = _G
module ( "ai" )

StopG = {}
StopGMT = { __index = StopG }

base.setmetatable ( StopG, { __index = Goal } )

StopG.create = function ( time, elapsed )
  local goal = {}
  base.setmetatable ( goal, StopGMT )

  goal.time = time or 0
  goal.elapsed = elapsed or 0

  return goal
end

StopG.destroy = function ( self )
  self.time = nil
  self.elapsed = nil
end

StopG.reset = function ( self )
  self.elapsed = 0
end

StopG.process = function ( self, dt, agent )
  --local av = self:GetCorrectedAV ( 0, dt )
  --self.b2body:SetAngularVelocity ( av )
  local lvx, lvy = agent:GetCorrectedLV ( 0, 0, dt )
  agent:SetLinearVelocity ( lvx, lvy )

  self.elapsed = self.elapsed + dt
  if self.elapsed >= self.time then
    local used = dt - ( self.elapsed - self.time )
    self.elapsed = 0
    return completed, used
  end
  return active, dt
end

Stop = StopG.create

-- XML
StopG.getType = function ( self )
  return "Stop"
end

StopG.createXML = function ( g )
  local time = base.tonumber ( g.time )
  local elapsed = base.tonumber ( g.elapsed )
  return StopG.create ( time, elapsed )
end
