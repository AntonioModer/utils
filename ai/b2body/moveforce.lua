local base = _G
module ( "ai" )

MoveForceG = {}
MoveForceGMT = { __index = MoveForceG }

base.setmetatable ( MoveForceG, { __index = Goal } )

MoveForceG.create = function ( x, y, speed, maxForce, ease, offset, tx, ty )
  local goal = {}
  base.setmetatable ( goal, MoveForceGMT )

  goal.x = x
  goal.y = y
  goal.speed = speed
  goal.maxForce = maxForce
  goal.ease = ease or false
  goal.offset = offset or "world"
  goal.tx = tx
  goal.ty = ty

  return goal
end

MoveForceG.destroy = function ( self )
  self.x = nil
  self.y = nil
  self.speed = nil
  self.maxForce = nil
  self.ease = nil
  self.offset = nil
  self.tx = nil
  self.ty = nil
end

MoveForceG.reset = function ( self )
  self.tx = nil
  self.ty = nil
end

MoveForceG.process = function ( self, dt, agent )
  if self.tx == nil or self.ty == nil then
    if self.offset == "world" then
      self.tx, self.ty = self.x, self.y
    elseif self.offset == "self" then
      self.tx, self.ty = agent:GetWorldPoint ( self.x, self.y )
    else
      local offset = agent.world:FindBody ( self.offset )
      if offset == nil or offset.b2body == nil then
        return failed, 0
      end
      self.tx, self.ty = offset:GetWorldPoint ( self.x, self.y )
    end
  end

  local x, y = self.tx, self.ty
  local cp = agent.def.position
  local clvx, clvy = agent.b2body:GetLinearVelocityXY ( 0, 0 )
  local cSpeed = math.sqrt ( clvx * clvx + clvy * clvy )

  -- condition
  local eta
  if cSpeed > 0 then
    eta = agent:GetArriveTime ( x, y, cSpeed )
    -- target will be reached within the next frame
    if eta < dt then
      agent:SetPosition ( x, y )
      self:reset ( )
      return completed, eta
    end
  else
    -- already on top of the target
    if x == cp.x and y == cp.y then
      self:reset ( )
      return completed, 0
    end
  end

	-- output
	if self.ease == true then
		if cSpeed > 0 then
			-- time required for a full stop
			local sx, sy = agent:GetCorrectedLV ( 0, 0, eta )
			local stopTime = agent:GetAccelerationTime ( sx, sy, self.maxForce, clvx, clvy )
			--local stopTime = agent:GetAccelerationTime ( base.b2_b2Vec2, self.maxForce, currentLV )

			-- ease if we are going to overshoot the target
			if eta <= stopTime then
				-- desired velocity = ( final position - initial position ) / time
				local dx, dy = agent:GetFuturePosition ( stopTime, clvx, clvy )

				dx = ( x - dx ) / stopTime
				dy = ( y - dy ) / stopTime
				--d = agent:GetCorrectedLV ( d, dt )

				-- force = ( velocity / time ) * mass
				local mass = agent.b2body:GetMass ( )
				local fx = ( dx / dt ) * mass
				local fy = ( dy / dt ) * mass

				-- truncate to maximum steering force
				local fd = math.sqrt ( fx * fx + fy * fy )
				if fd > self.maxForce then
				  fx = fx / fd * self.maxForce
				  fy = fy / fd * self.maxForce
				end
				agent:ApplyForce ( fx, fy )

				return active, dt
			end
		end
	end

	-- desired velocity
  local dx, dy = x - cp.x, y - cp.y
  local d = math.sqrt ( dx * dx + dy * dy )
  dx, dy = dx / d * self.speed, dy / d * self.speed

	dx, dy = agent:GetCorrectedLV ( dx, dy, dt )

  local fx, fy = agent:GetAccelerationForce ( dt, dx, dy, clvx, clvy )
  local fd = math.sqrt ( fx * fx + fy * fy )
  if fd > self.maxForce then
    fx = fx / fd * self.maxForce
    fy = fy / fd * self.maxForce
  end
  agent:ApplyForce ( fx, fy )

	return active, dt
end

MoveForce = MoveForceG.create

-- XML
MoveForceG.getType = function ( self )
  return "MoveForce"
end

MoveForceG.createXML = function ( g )
  local x = base.tonumber ( g.x )
  local y = base.tonumber ( g.y )
  local speed = base.tobnumber ( g.speed )
  local maxForce = base.tonumber ( g.maxForce )
  local ease = base.toboolean ( g.ease )
  local tx = tonumber ( g.tx )
  local ty = tonumber ( t.gy )
  return MoveForceG.create ( x, y, speed, maxForce, ease, g.offset, tx, ty )
end