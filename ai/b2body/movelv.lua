local base = _G
module ( "ai" )

MoveLVG = {}
MoveLVGMT = { __index = MoveLVG }

base.setmetatable ( MoveLVG, { __index = Goal } )

MoveLVG.create = function ( x, y, speed, offset, tx, ty )
  local goal = {}
  base.setmetatable ( goal, MoveLVGMT )

  goal.x = x
  goal.y = y
  goal.speed = speed
  goal.offset = offset or 'world'
  goal.tx = tx
  goal.ty = ty

  return goal
end

MoveLVG.destroy = function ( self )
  self.x = nil
  self.y = nil
  self.speed = nil
  self.offset = nil
  self.tx = nil
  self.ty = nil
end

MoveLVG.reset = function ( self )
  self.tx = nil
  self.ty = nil
end

MoveLVG.process = function ( self, dt, agent )
  if self.tx == nil or self.ty == nil then
    if self.offset == "world" then
      self.tx, self.ty = self.x, self.y
    elseif self.offset == "self" then
      self.tx, self.ty = agent:GetWorldPoint ( self.x, self.y )
    end
  end
  if self.offset ~= "world" and self.offset ~= "self" then
    local offset = agent.world:FindBody ( self.offset )
    if offset == nil or offset.b2body == nil then
      return "failed", 0
    end
    self.tx, self.ty = offset:GetWorldPoint ( self.x, self.y )
  end
    
--[[
  if self.tx == nil or self.ty == nil then
    if self.offset == "world" then
      self.tx, self.ty = self.x, self.y
    elseif self.offset == "self" then
      self.tx, self.ty = agent:GetWorldPoint ( self.x, self.y )
    else
      local offset = agent.world:FindBody ( self.offset )
      if offset == nil or offset.b2body == nil then
        return failed, 0
      end
      self.tx, self.ty = offset:GetWorldPoint ( self.x, self.y )
    end
  end
]]

  local x, y = self.tx, self.ty
  -- condition
  local eta = agent:GetArriveTime ( x, y, self.speed )
  if eta < dt then
    agent:SetPosition ( x, y )
    self:reset ( )
    return "completed", eta
  end
  
  -- output
  local cp = agent.def.position
  local dx, dy = x - cp.x, y - cp.y
  local d = math.sqrt ( dx * dx + dy * dy )
  dx, dy = dx / d * self.speed, dy / d * self.speed

  dx, dy = agent:GetCorrectedLV ( dx, dy, dt )
  agent:SetLinearVelocity ( dx, dy )

  return "active", dt
end

MoveLV = MoveLVG.create

-- XML
MoveLVG.getType = function ( self )
  return "MoveLV"
end

MoveLVG.createXML = function ( g )
  local x = base.tonumber ( g.x )
  local y = base.tonumber ( g.y )
  local speed = base.tobnumber ( g.speed )
  local tx = tonumber ( g.tx )
  local ty = tonumber ( t.gy )
  return MoveLVG.create ( x, y, speed, g.offset, tx, ty )
end