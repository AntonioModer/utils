local base = _G
module ( "ai" )

RotateTorqueG = {}
RotateTorqueGMT = { __index = RotateTorqueG }

base.setmetatable ( RotateTorqueG, { __index = Goal } )

RotateTorqueG.create = function ( angle, turnRate, maxTorque, ease )
  local goal = {}
  base.setmetatable ( goal, RotateTorqueGMT )

  goal.angle = angle
  goal.turnRate = turnRate
  goal.maxTorque = maxTorque
  goal.ease = ease or false

  return goal
end

RotateTorqueG.destroy = function ( self )
  self.angle = nil
  self.turnRate = nil
  self.maxTorque = nil
  self.ease = nil
end

RotateTorqueG.process = function ( self, dt, agent )
	local currentAngle = agent.def.angle
	local desiredAV = self.turnRate
	local angle = self.angle
	local maxTorque = self.maxTorque
	if currentAngle > angle then
		desiredAV = -desiredAV
	end
	local currentAV = agent.b2body:GetAngularVelocity ( )

	-- condition
	if currentAV ~= 0 then
		-- eta is negative when the body is rotating in the wrong direction
		local eta = agent:GetTurnTime ( angle, currentAV )
--eta = math.abs ( eta )
		if eta >= 0 and eta < dt then
			agent:SetAngle ( angle )
--[[
-- todo
if ease == true then
    agent.b2body:SetAngularVelocity ( 0 )
end
]]
			return completed, eta
		end
	else
		if angle == currentAngle then
			return completed, 0
		end
	end

	-- output
	if ease == true then
		if currentAV ~= 0 then
			-- time required for a full stop
			local stopTime = agent:GetAngularAccelerationTime ( 0, maxTorque, currentAV )
			stopTime = math.abs ( stopTime )

			local eta = agent:GetTurnTime ( angle, currentAV )
--eta = math.abs ( eta )
			-- will we overshoot the target?
			if eta >= 0 and eta <= stopTime then

				local futureAngle = agent:GetFutureAngle ( stopTime, currentAV )
				-- desired velocity = ( final angle - initial angle ) / time
				desiredAV = ( angle - futureAngle ) / stopTime
				desiredAV = agent:GetCorrectedAV ( desiredAV, dt )

				-- torque = ( velocity / time ) * inertia
				local inertia = agent.b2body:GetInertia ( )
				local torque = ( desiredAV / dt ) * inertia

				-- truncate to maximum torque
				--torque = math.clamp ( torque, -maxTorque, maxTorque )
				torque = b2Clamp ( torque, -maxTorque, maxTorque )

				agent.b2body:ApplyTorque ( torque )

				return active, dt
			end
		end
	end

  local torque = agent:GetAccelerationTorque ( dt, desiredAV, currentAV )
  -- truncate to maximum torque
  --torque = math.clamp ( torque, -maxTorque, maxTorque )
  torque = base.b2Clamp ( torque, -maxTorque, maxTorque )
  agent.b2body:ApplyTorque ( torque )
  --agent:AccelerateAV ( dt, desiredAV, maxTorque )

	return active, dt
end

RotateTorque = RotateTorqueG.create


-- XML
RotateTorqueG.getType = function ( self )
  return "RotateTorque"
end

RotateTorqueG.createXML = function ( g )
  local angle = base.tonumber ( g.angle )
  local turnRate = base.tonumber ( g.turnRate )
  local maxTorque = base.tonumber ( g.maxTorque )
  local ease = base.toboolean ( g.ease )
  return RotateTorqueG.create ( angle, turnRate, maxTorque, ease )
end



