local base = _G
module ( "ai" )

MoveG = {}
MoveGMT = { __index = MoveG }

base.setmetatable ( MoveG, { __index = Goal } )

MoveG.create = function ( x, y, speed, offset, tx, ty )
  local goal = {}
  base.setmetatable ( goal, MoveGMT )

  goal.x = x
  goal.y = y
  goal.speed = speed
  goal.offset = offset or "world"
  goal.tx = tx
  goal.ty = ty

  return goal
end

MoveG.destroy = function ( self )
  self.x = nil
  self.y = nil
  self.speed = nil
  self.offset = nil
  self.tx = nil
  self.ty = nil
end

MoveG.reset = function ( self )
  self.tx = nil
  self.ty = nil
end

MoveG.process = function ( self, dt, agent )
  if self.tx == nil or self.ty == nil then
    if self.offset == "world" then
      self.tx, self.ty = self.x, self.y
    elseif self.offset == "self" then
      self.tx, self.ty = agent:GetWorldPoint ( self.x, self.y )
    else
      local offset = agent.world:FindBody ( self.offset )
      if offset == nil or offset.b2body == nil then
        return failed, 0
      end
      self.tx, self.ty = offset:GetWorldPoint ( self.x, self.y )
    end
  end

  local x, y = self.tx, self.ty
  -- teleport
  if self.speed == nil or self.speed == 0 then
    agent:SetPosition ( x, y )
    self:reset ( )
    return completed, 0
  end

  -- condition
  local eta = agent:GetArriveTime ( x, y, self.speed )
  if eta < dt then
    agent:SetPosition ( x, y )
    self:reset ( )
    return completed, eta
  end

  -- output
  local cp = agent.def.position
  local dx = x - cp.x
  local dy = y - cp.y
  local d = math.sqrt ( dx * dx + dy * dy )
  local step = self.speed * dt
  dx, dy = dx / d * step, dy / d * step
  agent:SetPosition ( cp.x + dx, cp.y + dy )

  -- we are moving the body manually so clear all forces acting upon it
  local lvx, lvy = agent:GetCorrectedLV ( 0, 0, dt )
  agent:SetLinearVelocity ( lvx, lvy )

  return active, dt
end

Move = MoveG.create


-- XML
MoveG.getType = function ( self )
  return "Move"
end

MoveG.createXML = function ( g )
  local x = base.tonumber ( g.x )
  local y = base.tonumber ( g.y )
  local speed = base.tonumber ( g.speed )
  local tx = base.tonumber ( g.tx )
  local ty = base.tonumber ( g.ty )
  return MoveG.create ( x, y, speed, g.offset, tx, ty )
end