local base = _G
module ( "ai" )

RotateAVG = {}
RotateAVGMT = { __index = RotateAVG }

base.setmetatable ( RotateAVG, { __index = Goal } )

RotateAVG.create = function ( angle, turnRate )
  local goal = {}
  base.setmetatable ( goal, RotateAVGMT )

  goal.angle = angle
  goal.turnRate = turnRate

  return goal
end

RotateAVG.destroy = function ( self )
  self.angle = nil
  self.turnRate = nil
end

RotateAVG.process = function ( self, dt, agent )
  local currentAngle = agent.def.angle
  local angle = self.angle
  local desiredAV = self.turnRate
  if currentAngle > angle then
    desiredAV = -desiredAV
  end
  -- condition
  local eta = agent:GetTurnTime ( angle, desiredAV )
  if eta < dt then
    agent:SetAngle ( angle )
    return completed, eta
  end
  -- output
  desiredAV = agent:GetCorrectedAV ( desiredAV, dt )
  agent.b2body:SetAngularVelocity ( desiredAV )
  return active, dt
end

RotateAV = RotateAVG.create

-- XML
RotateAVG.getType = function ( self )
  return "RotateAV"
end

RotateAVG.createXML = function ( g )
  return RotateAVG.create ( g.angle, g.turnRate )
end
