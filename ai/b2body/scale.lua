local base = _G
module ( "ai" )

ScaleG = {}
ScaleGMT = { __index = ScaleG }

base.setmetatable ( ScaleG, { __index = Goal } )

ScaleG.create = function ( scale, time, elapsed )
  local goal = {}
  base.setmetatable ( goal, ScaleGMT )

  goal.scale = scale
  goal.time = time or 0
  goal.elapsed = elapsed or 0

  return goal
end

ScaleG.destroy = function ( self )
  self.scale = nil
  self.time = nil
  self.elapsed = nil
end

ScaleG.reset = function ( self )
  self.elapsed = 0
end

ScaleG.process = function ( self, dt, agent )
  self.elapsed = self.elapsed + dt

  -- condition
  if self.elapsed >= self.time then
    agent:SetScale ( self.scale )
    local used = dt - ( self.elapsed - self.time )
    self.elapsed = 0
    return completed, used
  end

  -- output
  local ratio = dt / ( self.time - ( self.elapsed - dt ) )
  agent:SetScale ( agent.scale + ( self.scale - agent.scale ) * ratio )

  return active, dt
end

Scale = ScaleG.create


-- XML
ScaleG.getType = function ( self )
  return "Scale"
end

ScaleG.createXML = function ( g )
  local scale = base.tonumber ( g.scale )
  local time = base.tonumber ( g.time )
  local elapsed = base.tonumber ( g.elapsed )
  return ScaleG.create ( scale, time, elapsed )
end


