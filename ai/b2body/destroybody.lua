local base = _G
module ( "ai" )

DestroyG = {}
DestroyGMT = { __index = DestroyG }

base.setmetatable ( DestroyG, { __index = Goal } )

DestroyG.create = function ( name )
  local goal = {}
  base.setmetatable ( goal, DestroyGMT )

  goal.name = name

  return goal
end

DestroyG.destroy = function ( self )
  self.name = nil
end

DestroyG.process = function ( self, dt, agent )
  local body = getComponent ( agent, "body" )
  local world = body:GetWorld ( )
  world:DestroyBody ( body )
  return "completed", 0
end

Destroy = DestroyG.create

--
-- XML
--
DestroyG.getType = function ( self )
  return "Destroy"
end

DestroyG.createXML = function ( g )
  return DestroyG.create ( g.name )
end