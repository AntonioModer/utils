display:create("AI Example", 800, 600, 32, true)

require("ai.main")

local a = {}

ai.addAgent(1, a)

local s = Sprite()
s.canvas:square(64)
s.canvas:fill()
display.viewport:add_child(s)

ai.setComponent(1, "scenenode", s)

local r = ai.Sequence(true)
r:push(ai.Tween("rotation", 180, 1))
r:push(ai.Tween("rotation", 0, 1))

local m = ai.Sequence(true)
m:push(ai.TweenXY("x", "y", -200, 0, 200))
m:push(ai.TweenXY("x", "y", 200, 0, 200))
m:push(ai.TweenXY("x", "y", 0, 0, 200))
m:push(ai.Wait(1))

-- no way I'm fixing this mess :(
local c = ai.Sequence(true)
c:push(ai.Modulate(RED, 2))
c:push(ai.Modulate(GREEN, 2))
c:push(ai.Modulate(BLUE, 2))
c:push(ai.Modulate(WHITE, 2))

local g = ai.Parallel(true)
g:push(r)
g:push(m)
g:push(c)

ai.setGoal(1, g)

timer = Timer()
timer:start(16, true)
timer.on_tick = function(timer)
  local dt = timer:get_delta()*(timer.interval/1000)
  display:set_title(dt)
  ai.updateGoals(dt)
end