--
-- Executes a list of sub-goals in sequential order
-- 

local base = _G
local table = table
module("ai")

SelectorG = {}
SelectorGMT = { __index = SelectorG }

base.setmetatable(SelectorG, { __index = GoalG })

SelectorG.create = function(loop, mt)
  local goal = Goal(mt or SelectorGMT)

  goal.goals = {}
  goal.loop = loop

  return goal
end

SelectorG.destroy = function(self)
  self:clear()

  self.goals = nil
  self.loop = nil
  
  GoalG.destroy(self)
end

SelectorG.reset = function(self)
  if self.active ~= nil then
    self.goals[self.active]:reset()
    self.active = nil
  end
  self.first = nil
end

SelectorG.process = function(self, dt, agent)
  return self:processSubGoals(dt, agent)
end

SelectorG.processSubGoals = function(self, dt, agent)
  if #self.goals == 0 then
    return 'inactive', 0
  end

  -- activate if inactive
  if self.active == nil then
    self.active = 1
  end

  -- process the active goal
  local status, used = self.goals[self.active]:process(dt, agent)

  -- the active goal has destroyed this selector
  if self.active == nil then
    return 'completed', used
  end

  if status == 'active' then
    self.first = nil
    return 'active', used
  elseif status == 'failed' then
    -- remember the first goal that failed immediately
    if used == 0 then
      self.first = self.first or self.active
    else
      self.first = nil
    end

    -- move to the next goal
    self.active = self.active + 1
    if self.active > #self.goals then
      if self.loop ~= true then
        -- the selector has failed
        self.active = nil
        self.first = nil
        return 'failed', used
      end
      self.active = 1
    end

    -- all goals have failed immediately in a loop
    if self.loop == true then
      if self.first == self.active and used == 0 then
        return 'active', used
      end
    end

    if dt == used then
      self.first = nil
      return 'active', used
    end

    return self:process(dt - used, agent)
  elseif status == completed then
    -- the selector is complete
    self.active = nil
    self.first = nil
    return 'completed', used
  end
end

SelectorG.push = function(self, goal)
  base.assert(goal)
  table.insert(self.goals, goal)
end

SelectorG.clear = function(self)
  self:reset()
  while #self.goals > 0 do
    local last = table.remove(self.goals)
    last:Destroy()
  end
end

SelectorG.message = function(self, agent, msg, ...)
  if self.active then
    return self.goals[self.active]:message(agent, msg, ...)
  end
  return false
end

Selector = SelectorG.create



-- XML
SelectorG.getType = function(self)
  return "Selector"
end

SelectorG.createXML = function (goal)
  local loop = base.toboolean(goal.loop)
  local g = SequenceG.create(loop)
  g.active = base.tonumber(goal.active)
  for i = 1, #goal, 1 do
    local sub = GoalG.createXML(goal[i])
    table.insert(g.goals, sub)
  end
  return g
end

SelectorG.getXML = function(self)
  local g = base.xml.new("Goal")
  g.type = self:getType()
  g.active = self.active
  g.loop = self.loop
  for i, v in base.ipairs(self.goals) do
    local sub = v:getXML()
    g:append(sub)
  end
  return g
end
