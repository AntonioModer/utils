--[[
Copyright (c) 2011,2013 Robin Wellner
Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

-- localize common functions
local pairs = pairs
local tostring = tostring
local type = type
local concat = table.concat
local dump = string.dump
local format = string.format
local gsub = string.gsub
local byte = string.byte
local sub = string.sub
local match = string.match

-- localize internal data so we don't
-- have to pass it between functions all the time
local memo = nil
local rmemo = nil
local srefs = nil

local function getchr(c)
  return "\\" .. byte(c)
end

local function make_safe(text)
  text = format("%q", text)
  text = gsub(text, "\n", "n")
  return gsub(text, "[\128-\255]", getchr)
end

local oddvals = {[tostring(1/0)] = '1/0', [tostring(-1/0)] = '-1/0', [tostring(0/0)] = '0/0'}

local function write_type(t, ty)
  if ty == 'number' then
    --t = format("%.17g", t) -- inaccurate for floats
    return oddvals[t] or t
  elseif ty == 'boolean' or ty == 'nil' then
    return tostring(t)
  elseif ty == 'string' then
    return make_safe(t)
  elseif ty == 'table' or ty == 'function' then
    local m = memo[t]
    if not m then
      m = #rmemo + 1
      memo[t] = m
      rmemo[m] = t
    end
    return '_[' .. m .. ']'
  else
    error("Trying to serialize unsupported type " .. ty)
  end
end

local kw = {['and'] = true, ['break'] = true, ['do'] = true, ['else'] = true,
  ['elseif'] = true, ['end'] = true, ['false'] = true, ['for'] = true,
  ['function'] = true, ['goto'] = true, ['if'] = true, ['in'] = true,
  ['local'] = true, ['nil'] = true, ['not'] = true, ['or'] = true,
  ['repeat'] = true, ['return'] = true, ['then'] = true, ['true'] = true,
  ['until'] = true, ['while'] = true}

local function write_pair(k, v, kt, vt)
  v = write_type(v, vt)
  if kt == 'string' and match(k, '^[_%a][_%w]*$') and not kw[k] then
    return k .. '=' .. v
  else
    k = write_type(k, kt)
    return '[' .. k .. ']=' .. v
  end
end

local function write_pair2(name, k, v, kt, vt)
  v = write_type(v, vt)
  if kt == 'string' and match(k, '^[_%a][_%w]*$') and not kw[k] then
    return '_[' .. name .. ']' .. '.' .. k ..'=' .. v
  else
    k = write_type(k, kt)
    return '_[' .. name .. ']' .. '[' .. k .. ']=' .. v
  end
end

-- fun fact: this function is not perfect
-- it has a few false positives sometimes
-- but no false negatives, so that's good
local function is_cyclic(sub, super)
  local m = memo[sub]
  local p = memo[super]
  return m and p and m < p
end

local function write_table(t, name)
  if type(t) == 'function' then
    return '_[' .. name .. ']=loadstring' .. make_safe(dump(t))
  end
  local m = {}
  local mi = 1
  local ri = #srefs + 1
  for i = 1, #t do -- don't use ipairs here, we need the gaps
    local v = t[i]
    if v == t or is_cyclic(v, t) then
      srefs[ri] = {name, i, v}
      ri = ri + 1
      m[mi] = 'nil'
      mi = mi + 1
    else
      m[mi] = write_type(v, type(v))
      mi = mi + 1
    end
  end
  for k, v in pairs(t) do
    local kt = type(k)
    --if kt ~= 'number' or k < 1 or k > #t or floor(k) ~= k then
    if kt ~= 'number' or k < 1 or k > #t or k%1 ~= 0 then
      if v == t or k == t or is_cyclic(v, t) or is_cyclic(k, t) then
        srefs[ri] = {name, k, v}
        ri = ri + 1
      else
        m[mi] = write_pair(k, v, kt, type(v))
        mi = mi + 1
      end
    end
  end
  -- separate pairs with comma
  return '_[' .. name .. ']={' .. concat(m, ',') .. '}'
end

return function(t)
  -- phase 0: initialize internal data
  memo = {[t] = 0}
  rmemo = {[0] = t}
  srefs = {}
  
  local result = {}

  -- phase 1: recursively descend the table structure
  local n = 0
  while rmemo[n] do
    local n1 = n + 1
    result[n1] = write_table(rmemo[n], n)
    n = n1
  end

  -- phase 2: reverse order
  for i = 1, n*0.5 do
    local j = n - i + 1
    result[i], result[j] = result[j], result[i]
  end

  -- phase 3: add all the tricky cyclic stuff
  for i = 1, #srefs do
    local s = srefs[i]
    local k, v = s[2], s[3]
    n = n + 1
    result[n] = write_pair2(s[1], k, v, type(k), type(v))
  end

  -- phase 4: add something about returning the main table
  if sub(result[n], 1, 5) == '_[0]=' then
    result[n] = 'return ' .. sub(result[n], 6)
  else
    result[n + 1] = 'return _[0]'
  end

  -- phase 5: just concatenate everything
  result = concat(result, '\n')
  
  -- phase 6: clean up
  memo = nil
  rmemo = nil
  srefs = nil
  
  return n > 1 and 'local _={}\n' .. result or result
end
