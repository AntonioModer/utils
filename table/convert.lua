--- Converts the string values in a table to numbers and booleans
-- @param s Source table
-- @param d Destination table (optional)
-- @return Destination table
local function convert(s, d)
  d = d or s
  for k, v in pairs(s) do
    local t = type(v)
    if t == "table" then
      -- recursive
      if type(d[k]) ~= 'table' then
        d[k] = {}
      end
      convert(v, d[k])
    elseif t == "string" then
      -- convert
      if v == "true" then
        v = true
      elseif v == "false" then
        v = false
      else
        v = tonumber(v) or v
      end
      d[k] = v
    end
  end
  return d
end

--- Replaces existing values in a table
-- @param s Source table
-- @param d Destination table
-- @return Destination table
local function replace(s, d)
  assert(s ~= d, "source and destination tables must be different")
  for k, dv in pairs(d) do
    local sv = s[k]
    if sv ~= nil and sv ~= dv then
      local dt = type(dv)
      local st = type(sv)
      if st == "table" then
        -- recursive
        if dt ~= "table" then
          d[k] = {}
        end
        replace(sv, dv)        
      else
        -- replace
        d[k] = sv
      end
    end
  end
  return d
end

table.convert = convert
table.replace = replace