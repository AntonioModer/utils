local floor = math.floor

local _C = {}

function _C.validate(y, m, d)
  if d < 1 or d > 31 or m < 1 or m > 12 then
    -- year, month, day out of range
    return false
  end
  if m == 4 or m == 6 or m == 9 or m == 11 then 
    -- Apr, Jun, Sep, Nov can have at most 30 days
    return d <= 30
  elseif m == 2 then
    -- Feb
    if y%400 == 0 or (y%100 ~= 0 and y%4 == 0) then
      -- if leap year, days can be at most 29
      return d <= 29
    else
      -- else 28 days is the max
      return d <= 28
    end
  else 
    -- all other months can have at most 31 days
    return true
  end
end

--[[
starts: January 1, 4713 BCE noon Universal Time
        the most recent year beginning with a full moon
]]
--- Gregorian calendar date to Julian
-- @d day (1-31)
-- @m month (1-12)
-- @y year (4 digits)
function _C.jdn(Y, M, D, h, m, s)
--[[
  if M < 3 then
    M = M + 12
    Y = Y - 1
  end
  local a = floor((153*M - 457)/5)
  local b = floor(Y/4)
  local c = floor(Y/100)
  local e = floor(Y/400)
  return D + a + 365*Y + b - c + e + 1721118.5
]]
----[[
  assert(_C.validate(Y, M, D))
  h = h or 12
  m = m or 0
  s = s or 0
  
  local _A = floor((14 - M)/12)
  local _Y = Y + 4800 - _A
  local _M = M + 12*_A - 3
  
  local _1 = floor((153*_M + 2)/5)
  local _2 = floor(_Y/4)
  local _3 = floor(_Y/100)
  local _4 = floor(_Y/400)
  
  local JDN = D + _1 + 365*_Y + _2 - _3 + _4 - 32045
  return JDN + (h - 12)/24 + m/1440 + s/86400
  --]]
end

function _C.gd(JDN)
  local _f = JDN + 1401
  _f = _f + floor((4*JDN + 274277)/146097*3/4) - 38
  local _e = 4*_f + 3
  local _g = _e%1461/4
  local _h = 5*_g + 2
  local D = _h%153/5 + 1
  local M = (_h/153 + 2)%12 + 1
  local Y = _e/1461 - 4716 + (12 + 2 - M)/12

  return Y, M, D
end

--[[
--- Gregorian calendar difference in years, months, weeks and days
-- @ts1 first timestamp
-- @ts2 second timestamp
-- @fdo first day of the week (0-6 where 0 is Sunday)
function _C.gdiff(ts1, ts2, fdo)
  local t1 = os.date("*t", ts1)
  local t2 = os.date("*t", ts2)
  -- in years
  local year = t1.year - t2.year
  -- in months
  local month = year*12 + (t1.month - t2.month)
  -- in days
  local j1 = _C.g2j(t1.day, t1.month, t1.year)
  local j2 = _C.g2j(t2.day, t2.month, t2.year)
  local day = j1 - j2
  -- in weeks
  fdo = fdo or 0
  local jw1 = (j1 - t1.wday - 1 + fdo)/7
  local jw2 = (j2 - t2.wday - 1 + fdo)/7
  local week = jw1 - jw2
  return year, month, week, day
end

--- Time difference
-- @param ts1 first timestamp
-- @param ts2 second timestamp
function _C.tdiff(ts1, ts2)
  -- in seconds
  local second = ts1 - ts2
  -- in minutes
  local minute = floor(second/60)
  -- in hours
  local hour = floor(minute/60)
  return hour, second, minute
end
]]

return _C