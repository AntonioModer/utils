local inter = {}

local locale = "en"
local territory = "us"

--- Replaces format flags ({0}, {1}, {2}, etc) in the string
-- @param sz Text string
-- @param ... List of arguments
function inter.format(sz, ...)
  local n = select('#', ...)
  if n > 0 then
    for i = 1, n do
      local a = select(i, ...)
      local sz2 = string.format("{%d}", i - 1)
      sz = string.gsub(sz, sz2, a)
    end
  end
  return sz
end

--- Sets the current locale
-- @param l Locale name
function inter.set_locale(l)
  locale = l
end

--- Gets the current locale
-- @return Locale name
function inter.get_locale()
  return locale
end

--- Sets the current territory
-- @param t Territory
function inter.set_territory(t)
  territory = t
end

--- Gets the current territory
-- @return Territory
function inter.get_territory()
  return territory
end

local plural = require("utils.lang.data.plural")

--- Returns the plural form of a number
-- @param l Locale name
-- @param n Number
-- @return Plural form: "one", "two", "few", "many" or "other"
function inter.plural_locale(l, n)
  if plural[l] == nil then
    return "other"
  end
  return plural[l](n)
end

function inter.plural(l, n)
  return inter.plural_locale(locale, n)
end

local period = require("utils.lang.data.period")

local units =
{
  second = 1,
  minute = 60,
  hour = 60*60,
  day = 60*60*24,
  week = 60*60*24*7,
  month = 60*60*24*30,
  year = 60*60*24*365
}

--- Time period from seconds to years in the past or future
--- ex: "5 years ago", "2 hours ago", "in 5 minutes", "in 3 weeks"
-- @param l Locale
-- @param d Time difference in seconds, could be negative
-- @param u Display unit: "second", "hour", "minute", "hour", "day", "week", "month" or "year"
-- @return Time period string
function inter.period_locale(l, d, u)
  -- absolute value
  local ad = math.abs(d)

  -- display units
  if u == nil then
    if ad < 60 then
      u = "second"
    elseif ad < 60*60 then
      u = "minute"
    elseif ad < 60*60*24 then
      u = "hour"
    elseif ad < 60*60*24*7 then
      u = "day"
    elseif ad < 60*60*24*30 then
      u = "week"
    elseif ad < 60*60*24*365 then
      u = "month"
    else
      u = "year"
    end
  end
  assert(units[u], "invalid display unit")
  local du = ad/units[u]
  du = math.floor(du)
  
  -- future or past
  local rt = period[l]
  local t = rt.future[u]
  if d < 0 then
    t = rt.past[u]
  end

  -- plural
  local pl = inter.plural_locale(l, du)
  local i = t[pl] or t.other
  return inter.format(i, du)
end

function inter.period(d, u)
  return inter.period_locale(locale, d, u)
end

local relative = require("utils.lang.data.relative")
local firstday = require("utils.lang.data.firstday")


-- day offsets relative to Sunday
local offsets =
{
  mon = 1, tue = 2, wed = 3, thu = 4, fri = 5, sat = 6, sun = 0
}

--- Relative time in the past, future or present
--- ex: "2 years ago", "last year", "yesterday", "in 5 minutes", "tomorrow"
-- @param l Locale
-- @param ts1 Time stamp
-- @param u Display unit: "second", "hour", "minute", "hour", "day", "week", "month" or "year"
-- @return Relative time string
function inter.relative_locale(l, ts1, u)
  local ts2 = os.time()
  assert(ts1 and ts2, "invalid time stamp")

  -- first day of the week
  local fd = firstday[territory] or 0
  local fdo = offsets[fd]
  
  -- differences
  local y, M, w, d = datediff(ts1, ts2, fdo)
  local h, m, s = timediff(ts1, ts2)
  
  -- display units
  if u == nil then
    if y ~= 0 and math.abs(M) > 3 then
      u = 'year'
    elseif M ~= 0 and math.abs(w) > 3 then
      u = 'month'
    elseif w ~= 0 and math.abs(d) > 3 then
      u = 'week'
    elseif d ~= 0 then
      u = 'day'
    elseif h ~= 0 then
      u = 'hour'
    elseif m ~= 0 then
      u = 'minute'
    else
      u = 'second'
    end
  end
  assert(units[u], "invalid display unit")
  
  -- relative names
  -- ex: "yesterday", "today", "tomorrow"
  local r = relative[l]
  if u == 'second' and r.second[s] then
    return r.second[s]
  end
  if u == 'day' and r.day[d] then
    return r.day[d]
  end
  if u == 'week' and r.week[w] then
    return r.week[w]
  end
  if u == 'month' and r.month[M] then
    return r.month[M]
  end
  if u == 'year' and r.year[y] then
    return r.year[y]
  end

  -- duration names
  -- ex: "1 day ago", "in 0 days", "in 1 day"
  return inter.period_locale(l, ts1 - ts2, u)
end

function inter.relative(d, u)
  return inter.relative_locale(locale, d, u)
end

local time = require("utils.lang.data.time")

function inter.time_locale(l, ts)
  
end

local date = require("utils.lang.data.date")
local month = require("utils.lang.data.months")
local day = require("utils.lang.data.days")

local dayn =
{
  [1] = "sun", [2] = "mon", [3] = "tue", [4] = "wed", [5] = "thu", [6] = "fri", [7] = "sat"
}

--[[
%a	abbreviated weekday name (e.g., Wed)
%A	full weekday name (e.g., Wednesday)
%b	abbreviated month name (e.g., Sep)
%B	full month name (e.g., September)
%c	date and time (e.g., 09/16/98 23:48:10)
%d	day of the month (16) [01-31]
%H	hour, using a 24-hour clock (23) [00-23]
%I	hour, using a 12-hour clock (11) [01-12]
%M	minute (48) [00-59]
%m	month (09) [01-12]
%p	either "am" or "pm" (pm)
%S	second (10) [00-61]
%w	weekday (3) [0-6 = Sunday-Saturday]
%x	date (e.g., 09/16/98)
%X	time (e.g., 23:48:10)
%Y	full year (1998)
%y	two-digit year (98) [00-99]
%%	the character `%´
]]
function inter.date_locale(l, ts, f)
  assert(l, "locale is nil")
  ts = ts or os.time()
  f = f or "f"
  local sz = date[l][f]
  assert(sz)
  local t = os.date("*t", ts)

  -- year
  sz = string.gsub(sz, "yy", "%%Y")
  sz = string.gsub(sz, "y", "%%y")
  -- month
  sz = string.gsub(sz, "MMMM", "%%B")
  sz = string.gsub(sz, "MMM", "%%b")
  sz = string.gsub(sz, "MM", "%%N")
  sz = string.gsub(sz, "M", "%%n")
  -- week day
  sz = string.gsub(sz, "EEEE", "%%A")
  sz = string.gsub(sz, "EEE", "%%a")
  -- day
  sz = string.gsub(sz, "dd", "%%D")
  sz = string.gsub(sz, "d", "%%d")
  
  -- if the calendar does not provide cyclic year name data
  -- or if the year value to be formatted is out of the range of years
  -- for which cyclic name data is provided
  -- then numeric formatting is used (behaves like 'y')
  --sz = string.gsub(sz, "U", "%%y")
  
  local Y = t.year
  local y = string.format("%%2d", y)
  local n = t.month
  local N = string.format("%2d", n)
  local wd = t.wday
  local d = t.day
  local D = string.format("%2d", d)

  local sz1 = sz

  -- year
  sz = string.gsub(sz, "%%Y", Y)
  sz = string.gsub(sz, "%%y", y)
  -- month
  sz = string.gsub(sz, "%%B", month[l].w[n])
  sz = string.gsub(sz, "%%b", month[l].a[n])
  sz = string.gsub(sz, "%%N", N)
  sz = string.gsub(sz, "%%n", n)
  -- week day
  sz = string.gsub(sz, "%%A", day[l].w[wd])
  sz = string.gsub(sz, "%%a", day[l].a[wd])
  -- day
  sz = string.gsub(sz, "%%D", D)
  sz = string.gsub(sz, "%%d", d)

  
--[[
  -- numeric
  local m = t.month
  sz = string.gsub(sz, "MM", string.format("%2d", m))
  sz = string.gsub(sz, "M", m)
  local d = t.day
  sz = string.gsub(sz, "dd", string.format("%2d", d))
  sz = string.gsub(sz, "d", d)
  local y = t.year
  sz = string.gsub(sz, "yy", string.format("%2d", y))
  sz = string.gsub(sz, "y", y)
  -- strings
  -- wide month name
  sz = string.gsub(sz, "MMMM", month[l].w[m])
  -- abbreviated month name
  sz = string.gsub(sz, "MMM", month[l].a[m])
  --local dn = dayn[t.wday]
  local wd = t.wday
  -- wide week day name
  sz = string.gsub(sz, "EEEE", day[l].w[wd])
  -- abbreviated week day name
  sz = string.gsub(sz, "EEE", day[l].a[wd])
]]

  return date[l][f] .. '|' .. sz1 .. '|' .. sz
end

function inter.date(ts, f)
  return inter.date_locale(locale, ts, f)
end


return inter