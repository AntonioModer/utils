local list = {}

--- Replaces format flags ({0}, {1}, {2}, etc) in the string
-- @param sz Text string
-- @param ... List of arguments
function list.format(sz, ...)
  local n = select('#', ...)
  if n > 0 then
    for i = 1, n do
      local a = select(i, ...)
      local sz2 = string.format("{%d}", i - 1)
      sz = string.gsub(sz, sz2, a)
    end
  end
  return sz
end

--- Formats a list of items
--- ex: "1 and 2", "1, 2, and 3"
-- @param l table of items
-- @param t two parts ("{0} and {1}")
-- @param s start part ("{0}, {1}")
-- @param m middle part ("{0}, {1}")
-- @param e end part ("{0}, and {1}")
-- @return string
local listp = nil
function list.list_locale(l, t, s, m, e)
  if #l == 0 then
    -- zero items
    return ""
  elseif #l == 1 then
    -- one item
    return l[1]
  elseif #l == 2 and t then
    -- two items
    return list.format(t, t[1], t[2])
  else
    -- many items
    -- start
    local sz = inter.format(s, t[1], t[2])
    -- middle
    for i = 3, #t - 1 do
      sz = list.format(m, sz, t[i])
    end
    -- end
    return list.format(e, sz, t[#t])
  end
end

return list