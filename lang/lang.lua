require("unicode")
local utf8 = unicode.utf8

--local utf8 = require("utils.lang.utf8")

local text = {}
local locale = nil
local mappings = {}

local lang = {}

-- Replaces format flags ($1, $2, etc) in the string
-- @param sz Text string
-- @param ... List of arguments
function lang.format(sz, ...)
  local n = select('#', ...)
  if n > 0 then
    for i = 1, n do
      local a = select(i, ...)
      sz = string.gsub(sz, "$" .. i, a)
    end
  end
  return sz
end

-- Replaces characters in the string
-- @param sz Text string
-- @param map Mapping table
function lang.replace(sz, map)
  return utf8.replace(sz, map)
end

-- Maps all characters in the string
-- @param az Text string
-- @param map Mapping table
function lang.map(sz, map)
  for i = 1, utf8.len(sz) do
    local c = utf8.sub(sz, i, i)
    map[c] = c
  end
end

-- Gets the current locale
-- @return Current locale name or nil
function lang.get_locale()
  return locale
end

-- Sets the current locale
-- @param l Locale name or nil
-- @return True if available or false otherwise
function lang.set_locale(l)
  assert(type(l) == "string", "locale name must be a string")
  locale = l
  return lang.available(l)
end

-- Checks if the locale is available
-- @param l Locale name
-- @return True if available or false otherwise
function lang.available(l)
  assert(type(l) == "string", "locale name must be a string")
  return text[l] ~= nil
end

-- Gets the list of all available locales
-- @param out Locales list (optional)
-- @return Locales list
function lang.get_available(out)
  out = out or {}
  for n in pairs(text) do
    table.insert(out, n)
  end
  table.sort(out)
  return out
end

-- Sets the text for a key and locale
-- @param l Locale name
-- @param k Key name
-- @param sz Text string
function lang.set_text_locale(l, k, sz)
  assert(l ~= nil, "no locale selected")
  assert(type(l) == "string", "locale name must be a string")
  assert(type(k) == "string", "text key must be a string")
  assert(type(sz) == "string", "text must be string")
  if text[l] == nil then
    text[l] = {}
  end
  text[l][k] = sz
end

-- Sets the text for a key
-- @param k Key name
-- @param sz Text string
function lang.set_text(k, sz)
  lang.set_text_locale(locale, k, sz)
end

-- Gets the text for a key and locale
-- @param l Locale name
-- @param k Key name
-- @param ... Format arguments
function lang.get_text_locale(l, k, ...)
  assert(l ~= nil, "no locale selected")
  assert(type(l) == "string", "locale name must be a string")
  assert(type(k) == "string", "text key must be a string")
  if text[l] == nil then
    return ""
  end
  local sz = text[l][k]
  if sz == nil then
    return ""
  end
  -- replace $1, $2, etc
  sz = lang.format(sz, ...)
  -- replace special characters
  if mappings[l] then
    sz = lang.replace(sz, mappings[l])
  end
  return sz
end

-- Gets the text for a key
-- @param k Key name
-- @param ... Format arguments
function lang.get_text(k, ...)
  return lang.get_text_locale(locale, k, ...)
end

-- Gets all key names for a locale
-- @param l Locale name
-- @return List of keys
function lang.get_keys_locale(l)
  assert(l ~= nil, "no locale selected")
  assert(type(l) == "string", "locale name must be a string")
  local out = {}
  if text[l] then
    for k in pairs(text[l]) do
      table.insert(out, k)
    end
  end
  return out
end

-- Gets all key names for a locale
-- @param l Locale name
-- @return List of keys
function lang.get_keys()
  return lang.get_keys_locale(locale)
end

--- Returns an iterator of keys and text for a locale
-- @return Iterator
function lang.keys_locale(l)
  assert(l ~= nil, "no locale selected")
  assert(type(l) == "string", "locale name must be a string")
  local all = lang.get_keys_locale(l)
  local i = 0
  return function()
    i = i + 1
    if i > #all then
      return
    end
    local k = keys[i]
    if text[l] and text[l][k] then
      return k, text[l][k]
    end
  end
end

--- Returns an iterator of keys and text
-- @return Iterator
function lang.keys()
  return lang.keys_locale(l)
end

-- Sets the mapping table for a locale
-- @param l Locale name
-- @param s First mapping string
-- @param d Second mapping string
function lang.set_mapping_locale(l, s, d)
  if s == nil or d == nil then
    mappings[l] = nil
    return
  end
  local l1, l2 = utf8.len(s), utf8.len(d)
  assert(l1 == l2, "mapping strings must be of equal length")
  local map = {}
  for i = 1, utf8.len(s) do
    local c1 = utf8.sub(s, i)
    local c2 = utf8.sub(d, i)
    map[c1] = c2
  end
  mappings[l] = map
end

-- Sets the mapping table
-- @param s First mapping string
-- @param d Second mapping string
function lang.set_mapping(s, d)
  lang.set_mapping_locale(locale, s, d)
end

-- Gets all characters used in a locale
-- @param l Locale name
-- @param list List of characters (optional)
-- @return List of characters
function lang.get_samplar_locale(l, list)
  assert(l ~= nil, "no locale selected")
  assert(type(l) == "string", "locale name must be a string")
  -- map all text
  local map = {}
  if text[l] then
    for _, sz in pairs(text[l]) do
      lang.map(sz, map)
    end
  end
  -- handle lower and upper case
  for c in pairs(map) do
    local lc = utf8.lower(c)
    local uc = utf8.upper(c)
    map[lc] = lc
    map[uc] = uc
  end
  -- convert the map to a list
  list = list or {}
  for b in pairs(map) do
    table.insert(list, b)
  end
  return list
end

-- Gets all characters used
-- @param list List of characters (optional)
-- @return List of characters
function lang.get_samplar(list)
  return lang.get_samplar_locale(locale, list)
end

function lang.get_codes_locale(l, list)
  list = lang.get_samplar_locale(l, list)
  for i = 1, #list do
    list[i] = utf8.byte(list[i])
  end
  return list
end

function lang.get_codes(list)
  return lang.get_codes_locale(locale, list)
end

-- Sets keys and text from a table for a locale
-- @param l Locale
-- @param t Table of keys and strings
function lang.import_locale(l, t)
  assert(t, "table is nil")
  for k, v in pairs(t) do
    lang.set_text_locale(l, k, v)
  end
end

-- Sets keys and text from a table
-- @param t Table of keys and strings
function lang.import(t)
  lang.import_locale(locale, t)
end

return lang