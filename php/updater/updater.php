<?php
  $vn = NULL;
  if (isset($_GET["version"])) $vn = $_GET["version"];
  $version = str_replace("../", "", $vn);
  $fn = NULL;
  if (isset($_GET["filename"])) $fn = $_GET["filename"];
  $file = str_replace("../", "", $fn);
  $pf = NULL;
  if (isset($_GET["platform"])) $pf = $_GET["platform"];
  $platform = str_replace("../", "", $pf);
  
  if (!$version)
  {
    //http_response_code(401);
    header('X-PHP-Response-Code: 401', true, 401);
    exit ("no version specified");
  }
  if (!$platform)
  {
    //http_response_code(401);
    header('X-PHP-Response-Code: 401', true, 401);
    exit ("no platform specified");
  }
  $dir = "files/$platform/$version";
  if (!is_dir($dir))
  {
    //http_response_code(401);
    header('X-PHP-Response-Code: 401', true, 401);
    exit ("no updates for the specified version/platform");
  }

  // get the required file
  if ($file)
  {
    if (!file_exists("$dir/$file"))
    {
      //http_response_code(404);
      header('X-PHP-Response-Code: 401', true, 401);
      exit("requested file is not available");
    }
    $f = file_get_contents("$dir/$file", 'r');
    //$h = fopen("files/$version/$file", 'rb');
    //$f = fread($h, filesize("files/$version/$file"));
    //fclose($h);
// Send rest of the file to the output buffer
//echo fpassthru($h);
//fclose($h);

    //$f = chunk_split($f, 76, "\r\n");
    header('Content-Type: text/plain');
    //header('Content-Transfer-Encoding: base64');

    echo base64_encode($f);
    //echo $f;
    exit;
  }
  
  // show a list of available updates for the given version
  function get_files($dir)
  {
    $root = scandir($dir);
    foreach($root as $value)
    {
      if($value === '.' || $value === '..') continue;
      if(is_file("$dir/$value"))
      {
        $result[]="$dir/$value";
        continue;
      }
      foreach(get_files("$dir/$value") as $value)
      {
        $result[]=$value;
      }
    }
    return $result;
  }
  
  $files = get_files($dir);
  if (count($files) == 0)
  {
    //http_response_code(401);
    header('X-PHP-Response-Code: 401', true, 401);
    exit ("no updates for the specified version");
  }
  echo "Available updates:\n";
  foreach($files as $f)
  {
    $fn = substr($f, strlen($dir) + 1);
    echo $fn, "\n";
    $ts = filemtime($f);
    echo date("Y/m/d H:i:s", $ts), "\n";
    echo md5_file($f), "\n";
  }
?>