--- Similar to xml.find but non recursive
--- will not find nested tags
-- @param s XML table
-- @param t Tag
-- @param a Attribute
-- @return Table or value
function xml.find2(s, t, a)
  --local r = xml.find(s, t, a)
  local r
  for i = 1, #s do
    local v = s[i]
    if v[0] == t then
      if a then
        v = v[a]
      end
      return v
    end
  end
end

function xml.find_number(s, t, a)
  local r = xml.find2(s, t, a or 1)
  if r then
    return tonumber(r)
  end
end

function xml.find_boolean(s, t, a)
  local r = xml.find2(s, t, a or 1)
  if r == "true" then
    return true
  elseif r == "false" then
    return false
  end
end

function xml.find_point(s, t)
  if t then
    s = xml.find2(s, t) or s
  end
  local x = xml.find2(s, "x", 1)
  local y = xml.find2(s, "y", 1)
  if x == nil or y == nil then
    return
  end
  return b2.Vec2(x, y)
end

function xml.find_path(s, t)
  if t then
    s = xml.find2(s, t) or s
  end
  local path = {}
  for i = 1, #s do
    assert(xml.tag(s[i]) == "Vertex", "unknown tag in path")
    local v = xml.find_point(s[i])
    assert(v, "invalid vertex in path")
    table.insert(path, v)
  end
  return path
end