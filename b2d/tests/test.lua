local maxsteps = 1 -- max steps per frame (aka max frameskip - 1)
local nparticles = 0 -- number of particles (initially, add more with the mouse wheel)
local profiling = nil -- profiling on? (toggle via the "P" key)
local pframes = 50 -- how often to reset/print the profiler (in frames)
local pwhat = nil
--local pwhat = "Lua"
--local pwhat = "C"
--local pwhat = "hooked"
--local pwhat = "internal"

display:create("terminal test", 1000, 600, 32, true, 30, false, 4)

terminal = require('utils.log.terminal')
profiler = require("utils.log.profile")
require("utils.b2d.main")
ddraw = require("utils.b2d.ddraw")

profiler.setclock(system.get_ticks)

profiler.hookall(pwhat)

t = Timer()
t:start(16, true)

w = b2.NewWorld()
w:SetGravityXY(0, -9.8)

local d, f, r = 0, 0, 0
bounds = w:NewBody("staticBody", 0, 0)
--bounds = w:NewBody("dynamicBody", 0,0)
--bounds:SetGravityScale(0)
-- borders
bounds:NewBox(0.1, 5, -5, 0, 0, d, f, r)
bounds:NewBox(0.1, 5, 5, 0, 0, d, f, r)
bounds:NewBox(10, 0.1, 0, 3, 0, d, f, r)
bounds:NewBox(10, 0.1, 0, -3, 0, d, f, r)
-- inclined surface
bounds:NewBox(5, 0.3, 0, -3, math.pi/4, d, f, r)

-- ball and box
local d2, f2, r2 = 5, 1, 0
ball = w:NewBody("dynamicBody", -2, -2)
ball:SetFixedRotation(true)
--ball:SetLinearVelocityXY(0, -2)
--ball:SetAngularVelocity(-40)
ball:NewCircle(0.5, 0, 0, d2, f2, r2)
--ball:NewBox(0.5, 0.5, 0, 0, 0, d2, f2, r2)
ball:SetBullet(true)

--ball:SetAngularDamping(1.5)

box = w:NewBody("dynamicBody", -2, 0)
box:SetFixedRotation(true)
--box:NewBox(0.5, 0.5, 0, 0, 0, d2, f2, r2)
box:NewCircle(0.5, 0, 0, d2, f2, r2)
box:SetBullet(true)

--[[
local v = {b2.Vec2(0,0), b2.Vec2(0.5,0), b2.Vec2(0.5,0.5)}
chain = w:NewBody("dynamicBody", -2, -0.5)
chain:NewChain(v, false, d2, f2, r2, false)
chain:SetBullet(true)

local v = {b2.Vec2(0,0), b2.Vec2(0.5,0), b2.Vec2(0.5,0.5)}
chain2 = w:NewBody("dynamicBody", -2.25, -0.5)
chain2:NewPolygon(v, d2, f2, r2, false, false)
chain2:SetBullet(true)


rope = w:NewBody("dynamicBody", -4, 1.5)
rope:NewBox(0.25, 0.1, 0, 0, 0, d2, f2, r2)
ropeJ = w:NewRopeJoint(bounds, rope, -4, 1, -4, 1.5, 1, true)

dist = w:NewBody("dynamicBody", -3, 1.5)
dist:NewBox(0.25, 0.1, 0, 0, 0, d2, f2, r2)
distJ = w:NewDistanceJoint(bounds, dist, -3, 1, -3, 1.5, true)

rotator = w:NewBody("dynamicBody", -2, 1)
rotator:NewBox(0.25, 0.1, 0, 0, 0, d2, f2, r2)
revoluteJ = w:NewRevoluteJoint(bounds, rotator, -2, 1, true)

slider = w:NewBody("dynamicBody", -1, 1)
slider:NewBox(0.1, 0.25, 0, 0, 0, d2, f2, r2)
prismaticJ = w:NewPrismaticJoint(bounds, slider, -1, 1, 0, 1, true)

gearJ = w:NewGearJoint(rotator, slider, revoluteJ, prismaticJ)

pulley1 = w:NewBody("dynamicBody", 0.25, 1)
pulley1:NewBox(0.1, 0.1, 0, 0, 0, d2, f2, r2)
pulley2 = w:NewBody("dynamicBody", -0.25, 1)
pulley2:NewBox(0.1, 0.1, 0, 0, 0, d2, f2, r2)
pulleyJ = w:NewPulleyJoint(pulley1, pulley2, 0.25, 1.5, -0.25, 1.5, 0.25, 1, -0.25, 1, 1, true)

welded = w:NewBody("dynamicBody", 1, 1)
welded:NewBox(0.1, 0.25, 0, 0, 0, d2, f2, r2)
weldJ = w:NewWeldJoint(bounds, welded, 1, 1, true)

-- Friction joint. 
-- This is used for top-down friction.
-- It provides 2D translational friction and angular friction. 
frict = w:NewBody("dynamicBody", 2, 1)
frict:SetGravityScale(0)
frict:NewBox(0.1, 0.25, 0, 0, 0, d2, f2, r2)
frictionJ = w:NewFrictionJoint(bounds, frict, 2, 1, true)
frictionJ:SetMaxForce(10)
frictionJ:SetMaxTorque(1)

wheel = w:NewBody("dynamicBody", 3, 1)
wheel:NewCircle(0.25, 0, 0, d2, f2, r2)
wheel:NewBox(0.4, 0.1, 0, 0, 0, d2, f2, r2)
wheelJ = w:NewWeldJoint(bounds, wheel, 3, 1, true)
]]

--[[
local t1 = system.get_ticks()
local m1 = collectgarbage("count")
for i = 1, 150000 do
  local x, y = box:GetPositionXY()
  local a = box:GetAngle()
end
local t2 = system.get_ticks()
local m2 = collectgarbage("count")

local t1b = system.get_ticks()
local m1b = collectgarbage("count")
for i = 1, 150000 do
  local xf = box:GetTransform()
  local p = xf.p
  
  local x, y = p.x, p.y
  local a = xf.q:GetAngle()
end
local t2b = system.get_ticks()
local m2b = collectgarbage("count")

terminal.print("getpos" .. t2 - t1 .. ' ' .. m2 - m1)
terminal.print("gettrans" .. t2b - t1b .. ' ' .. m2b - m1b)
]]

--box:ApplyLinearImpulseXY(0, -25500, -2, 0)
--ball:ApplyLinearImpulseXY(0, 25500, -2, 0)

local particles = {}
local function add_particles(n)
  -- particles
  for i = 1, n do
    local x = math.random()*8 - 4
    local y = math.random()*4 - 2
    local b = w:NewBody("dynamicBody", x,y)
    b:NewCircle(0.1, 0, 0, d2, f2, r2)
    particles[#particles + 1] = b
  end
end
local function remove_particles(n)
  for i = #particles, math.max(#particles - n, 1), -1 do
    local b = particles[#particles]
    w:DestroyBody(b)
    particles[#particles] = nil
  end
end
add_particles(nparticles)

-- visuals
local white = Color(255, 255, 255)
local red = Color(255, 0, 0)

local scale = 0.01
ddraw.set('line', scale)
ddraw.set('color', red)

scene = Layer()
scene:add_child(display.viewport.camera)
display.viewport.camera:set_scale(scale, scale)

overlay = Sprite()
scene:add_child(overlay)
overlay2 = Sprite()
display.viewport:add_child(overlay2)

font = Font()
font:load_system("Arial", 8)
overlay2.canvas:set_font(font, white, 1)

--[[
w:SetCallback("PostSolve", PostSolve)
w:SetCallback("PreSolve", PreSolve)
w:SetCallback("BeginContact", BeginContact)
]]

local velocity = b2.Vec2(0, 0)
function b2.Body:IsMoving(treshold)
  treshold = treshold or b2.linearSleepTolerance
  self:GetLinearVelocity(velocity)
  return velocity:LengthSquared() > treshold*treshold
end
function b2.Body:IsSpinning(treshold)
  treshold = treshold or b2.angularSleepTolerance
  local angular = self:GetAngularVelocity()
  return angular < -treshold or angular > treshold
end

local velocity = b2.Vec2(0, 0)
local gravity = w:GetGravity()
function b2.Body:IsGrounded(treshold)
  treshold = treshold or b2.linearSleepTolerance
  self:GetLinearVelocity(velocity)
  return angular < -treshold or angular > treshold
end
function b2.Body:IsConstrained()
  local ce = self:GetContactList()
  while ce do
    local ct = ce.contact
    if ct:IsTouching() then
      if not ct:GetFixtureA():IsSensor() then
        if not ct:GetFixtureB():IsSensor() then
          return true
        end
      end
    end
    ce = ce.next
  end
  return false
end

local wmf = b2.WorldManifold()
function b2.Body:IsPushedUp(ax, ay)
  if ax == nil then
    local g = w:GetGravity()
    ax, ay = g.x, g.y
  end
  local a = math.sqrt(ax*ax + ay*ay)
  local nax, nay = ax, ay --ax/a, ay/a
  
  --assert(nax*nax + nay*nay == 1)
  local edge = self:GetContactList()
  while edge do
    local contact = edge.contact
    if contact:IsTouching() then
      local mf = contact:GetManifold()
      local npoints = mf.pointCount
      if npoints > 0 then
        contact:GetWorldManifold(wmf)
        local n = wmf.normal
        local nx, ny = n.x, n.y
        local other = contact:GetFixtureA():GetBody()
        if other ~= self then
          nx, ny = -nx, -ny
        end
        local d = nax*nx + nay*ny
        if d > 0 then
          return true
        end
      end
    end
    edge = edge.next
  end
  return false
end


local _v1 = b2.Vec2(0, 0)
local _v2 = b2.Vec2(0, 0)
local wmf = b2.WorldManifold()
function b2.Body:GetContactImpulse()
  local ix, iy = 0, 0
  local ce = self:GetContactList()
  while ce do
    local contact = ce.contact
    assert(contact:GetFixtureA():GetBody() == self)
    assert(contact:GetFixtureB():GetBody() ~= self)
    if contact:IsTouching() then
      local manifold = contact:GetManifold()
      local npoints = manifold.pointCount
      if npoints > 0 then
        contact:GetWorldManifold(wmf)
        local n = wmf.normal
        local mp1 = manifold.points[1]
        local ni1 = mp1.normalImpulse
        local vx1, vy1 = self:GetLinearVelocityFromWorldPointXY(mp1.x, mp1.y)
        ix = ix + vx1
        iy = iy + vy1
        if npoints > 1 then
          local mp2 = manifold.points[2]
          local ni2 = mp1.normalImpulse
          local vx2, vy2 = self:GetLinearVelocityFromWorldPointXY(mp2.x, mp2.y)
          ix = ix + vx2
          iy = iy + vy2
        end
      end
    end
    ce = ce.next
  end
  return ix, iy
end

paused = false
accum = 0
int = 1/60
frame = 0
--collectgarbage("stop")
function t:on_tick()
  if paused then
    return    
  end
  postsolvers = 0
  frame = frame + 1
  if profiling then
    profiler.start()
  end
  
  local s = t:get_delta_ms()/1000

  accum = accum + s
  local steps = 0
  while accum >= int do
    w:Step(int, 8, 3)
    accum = accum - int
    steps = steps + 1
    if steps >= maxsteps then
      accum = 0
      break      
    end
  end

  if profiling then
    profiler.stop()
    if frame%pframes == 0 then
      terminal.clear()
      terminal.printf("profiling '%s': %d frames", profiling, pframes)
      local n = 1
      for f, c, t, d in profiler.query(profiling, 20) do
        --assert(f and f ~= "")
        terminal.printf("%d.'%s'x%d time:%f (%s)", n, f or "?", c, t, d or "?")
        --terminal.print(f .. " " .. (n or "?"))
        n = n + 1
      end
      profiler.reset()
    end
  else
    --terminal.clear()
  end

  terminal.trace("time", t:get_elapsed()/1000)
  terminal.trace("frame", frame)
  terminal.trace("steps", steps)
  terminal.trace("memory", collectgarbage("count"))
  terminal.trace("bodycount", w:GetBodyCount())
  terminal.trace("add/remove particles", "mouse wheel")
  terminal.trace("move bodies", "mouse")
  terminal.trace("toggle profiler", "'p' key (" .. (profiling or "off") .. ")")
  terminal.trace("show contacts", "'c' key")
  
  overlay2.canvas:clear()
  overlay2.canvas:set_font(font)
  overlay.canvas:clear()
  ddraw.draw_bodies(w, overlay.canvas, true)
  --ddraw.sync_bodies(w)
  ddraw.draw_joints(w, overlay.canvas)
  --ddraw.draw_contacts(w, overlay.canvas)
  
  --[[
  --terminal.printf("%d. STEP vel:%s,%s", frame, ball:GetLinearVelocityXY())
  local ce = ball:GetContactList()
  while ce do
    --DebugBallC(ce.contact)
    local mf = ce.contact:GetManifold()
    if mf.pointCount > 0 then
      ce.contact:GetWorldManifold(wmf)
      --terminal.printf("ti:%s", mf.points[1].tangentImpulse)
      --local p1 = wmf.points[1]
      --terminal.printf("v:%s, %s", ball:GetLinearVelocityFromWorldPointXY(p1.x, p1.y))
      
      local av = ball:GetAngularVelocity() -- - mf.points[1].tangentImpulse/ball:GetMass()
      av = av*ce.contact:GetFriction()
      terminal.printf("av:%s", av)
    end
    ce = ce.next
  end
  ]]
  
  --paused = true
end

function display.on_destroy()
  if profiling then
    --profiler.unhookall()
    profiler.stop()
    local out = io.open("profiler.txt", "w")
    local n = 1
    for f, c, t, d in profiler.query(profiling) do
      local sz = string.format("%d.'%s'x%d time:%s (%s)\n", n, f, c, t, d or "?")
      out:write(sz)
      n = n + 1
    end
  end
end

-- mouse joint movement
local mj = nil
function mouse:on_press()
  mouse.on_release()
  paused = false
  local mx, my = mouse.xaxis, mouse.yaxis
  local x,y = display.viewport.camera:get_world_point(mx, my)
  local q = w:QueryXY(x, y)
  for i = 1, #q do
    local a = q[i]:GetBody()
    if a:GetType() ~= "staticBody" then
      local x2,y2 = a:GetLocalPointXY(x,y)
      mj = w:NewMouseJoint(bounds, a, x, y, 200, true)
      break
    end
  end
end

function mouse:on_move()
  if mj then
    local x,y = display.viewport.camera:get_world_point(mouse.xaxis, mouse.yaxis)
    mj:SetTargetXY(x,y)
  end
end

function mouse:on_release()
  if mj then
    w:DestroyJoint(mj)
    mj = nil
  end
end

function mouse:on_wheelmove(z)
  if z > 0 then
    add_particles(20)
  else
    remove_particles(20)
  end
end

function keyboard:on_press(k)
  if k == KEY_P then
    if profiling == "time" then
      profiling = "calls"
      profiler.reset()
      terminal.clear()
    elseif profiling == "calls" then
      profiler.stop()
      profiling = nil
    else
      profiling = "time"
    end
  elseif k == KEY_C then
    local c = ddraw.get('contacts')
    ddraw.set('contacts', not c)
  end
end
