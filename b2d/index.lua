-- Access to bodies and joints via ID

local _bodies = {}
local _joints = {}
local _bodiesR = {}
local _jointsR = {}

function b2.World:Index()
  assert(_bodies[self] == nil, "world already indexed")
  _bodies[self] = {}
  _joints[self] = {}
  _bodiesR[self] = {}
  _jointsR[self] = {}
end

function b2.World:Deindex()
  assert(_bodies[self] ~= nil, "world already de-indexed")
  _bodies[self] = nil
  _joints[self] = nil
  _bodiesR[self] = nil
  _jointsR[self] = nil
end

-- gets body by id
function b2.World:GetBody(id)
  return _bodies[self][id]
end

-- reverse id find
function b2.World:GetBodyID(b2body)
  return _bodiesR[self][b2body]
end

-- set body id
function b2.World:SetBodyID(id, b2body)
  if b2body then
    if _bodies[self][id] then
      error("body id already taken: " .. id)
    end
    if _bodiesR[self][b2body] then
      error("body exists with another id: " .. id)
    end
    _bodiesR[self][b2body] = id
  end
  _bodies[self][id] = b2body
end

-- get first available ID
function b2.World:NextID()
  local b = _bodies[self]
  for i = 1, math.huge do
    if b[i] == nil then
      return i
    end
  end
end

-- get joint by id
function b2.World:GetJoint(id)
  return _joints[self][id]
end

-- reverse id find
function b2.World:GetJointID(b2joint)
  return _jointsR[self][b2joint]
end

-- set joint id
function b2.World:SetJointID(id, b2joint)
  if b2joint then
    if _joints[self][id] then
      error("joint id already taken: " .. id)
    end
    if _jointsR[self][b2joint] then
      error("joint exists with another id: " .. id)
    end
    _jointsR[self][b2joint] = id
  end
  _joints[self][id] = b2joint
end