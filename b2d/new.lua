-- Creating objects by reusing intermediate definitions

-- Vec2
local _vec = b2.Vec2(0, 0)
local _pt1 = b2.Vec2(0, 0)
local _pt2 = b2.Vec2(0, 0)
local _pt3 = b2.Vec2(0, 0)
local _pt4 = b2.Vec2(0, 0)
local _Vec2_Set = b2.Vec2.Set

-- World
function b2.NewWorld(gx, gy)
  _Vec2_Set(_vec, gx or 0, gy or 0)
  return b2.World(_vec)
end

-- Body
local _btypes =
{
  dynamicBody = true,
  staticBody = true,
  kinematicBody = true
}
local _BodyDef = b2.BodyDef()
local _BodyDef_Position = _BodyDef.position
local _World_CreateBody = b2.World.CreateBody
function b2.World:NewBody(t, x, y, a, ld, ad, fr, ib, as, aw, ac, gs)
  --assert(t, "body type is nil")
  --assert(_btypes[t], "invalid body type")
  --assert(x and y, "invalid body position")
  _BodyDef.type = t
  _Vec2_Set(_BodyDef_Position, x, y)
  _BodyDef.angle = a or 0
  _BodyDef.linearDamping = ld or 0
  _BodyDef.angularDamping = ad or 0
  _BodyDef.fixedRotation = fr or false
  _BodyDef.bullet = ib or false
  _BodyDef.allowSleep = as or false
  if aw == nil then
    aw = true
  end
  _BodyDef.awake = aw
  if ac == nil then
    ac = true
  end
  _BodyDef.active = ac
  _BodyDef.gravityScale = gs or 1
  return _World_CreateBody(self, _BodyDef)
end

-- Fixture
local _FixtureDef = b2.FixtureDef()
local _PolygonShape = b2.PolygonShape()
local _PolygonShape_Set = _PolygonShape.Set
local _Body_CreateFixture = b2.Body.CreateFixture
function b2.Body:NewPolygon(vertices, d, f, r, is)
  local vc = #vertices
  --assert(vc >= 2, "polygon with fewer than 2 vertices")
  --assert(vc <= b2.maxPolygonVertices, "polygon exceeds maxPolygonVertices")
  _PolygonShape_Set(_PolygonShape, vertices)
  _PolygonShape.vertexCount = vc
  _FixtureDef.density = d or 0
  _FixtureDef.friction = f or 0
  _FixtureDef.restitution = r or 0
  _FixtureDef.isSensor = is or false
  _FixtureDef.shape = _PolygonShape
  return _Body_CreateFixture(self, _FixtureDef)
end

local _PolygonShape_SetAsBox = _PolygonShape.SetAsBox
function b2.Body:NewBox(w, h, lx, ly, la, d, f, r, is)
  --assert(w > 0 and h > 0, "invalid box dimensions")
  _Vec2_Set(_vec, lx or 0, ly or 0)
  _PolygonShape_SetAsBox(_PolygonShape, w, h, _vec, la or 0)
  _FixtureDef.density = d or 0
  _FixtureDef.friction = f or 0
  _FixtureDef.restitution = r or 0
  _FixtureDef.isSensor = is or false
  _FixtureDef.shape = _PolygonShape
  return _Body_CreateFixture(self, _FixtureDef)
end


local _CircleShape = b2.CircleShape()
local _CircleShape_position = _CircleShape.position
function b2.Body:NewCircle(radius, lx, ly, d, f, r, is)
  --assert(radius > 0)
  _CircleShape.radius = radius
  _Vec2_Set(_CircleShape_position, lx or 0, ly or 0)
  _FixtureDef.density = d or 0
  _FixtureDef.friction = f or 0
  _FixtureDef.restitution = r or 0
  _FixtureDef.isSensor = is or false
  _FixtureDef.shape = _CircleShape
  return _Body_CreateFixture(self, _FixtureDef)
end

local _Chain_CreateLoop = b2.ChainShape.CreateLoop
local _Chain_CreateChain = b2.ChainShape.CreateChain
function b2.Body:NewChain(vertices, loop, d, f, r, is)
  local chain = b2.ChainShape()
  if loop == true then
    --assert(#vertices >= 3, "loop with fewer than 3 vertices")
    _Chain_CreateLoop(chain, vertices)
  else
    --assert(#vertices >= 2, "chain with fewer than 2 vertices")
    _Chain_CreateChain(chain, vertices)
  end
  _FixtureDef.density = d or 0
  _FixtureDef.friction = f or 0
  _FixtureDef.restitution = r or 0
  _FixtureDef.isSensor = is or false
  _FixtureDef.shape = chain
  return _Body_CreateFixture(self, _FixtureDef)
end

-- Joint
local _World_CreateJoint = b2.World.CreateJoint

local _RevoluteJointDef = b2.RevoluteJointDef()
local _RevoluteJointDef_Initialize = _RevoluteJointDef.Initialize
function b2.World:NewRevoluteJoint(a, b, x, y, cc)
  _Vec2_Set(_pt1, x, y)
  _RevoluteJointDef_Initialize(_RevoluteJointDef, a, b, _pt1)
  _RevoluteJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _RevoluteJointDef)
end

local _PrismaticJointDef = b2.PrismaticJointDef()
local _PrismaticJointDef_Initialize = _PrismaticJointDef.Initialize
function b2.World:NewPrismaticJoint(a, b, x, y, ax, ay, cc)
  _Vec2_Set(_pt1, x, y)
  _Vec2_Set(_pt2, ax, ay)
  _PrismaticJointDef_Initialize(_PrismaticJointDef, a, b, _pt1, _pt2)
  _PrismaticJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _PrismaticJointDef)
end

local _DistanceJointDef = b2.DistanceJointDef()
local _DistanceJointDef_Initialize = _DistanceJointDef.Initialize
function b2.World:NewDistanceJoint(a, b, p1x, p1y, p2x, p2y, cc)
  _Vec2_Set(_pt1, p1x, p1y)
  _Vec2_Set(_pt2, p2x, p2y)
  _DistanceJointDef_Initialize(_DistanceJointDef, a, b, _pt1, _pt2)
  _DistanceJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _DistanceJointDef)
end

local _RopeJointDef = b2.RopeJointDef()
local _RopeJointDef_localAnchorA = _RopeJointDef.localAnchorA
local _RopeJointDef_localAnchorB = _RopeJointDef.localAnchorB
local _body_GetLocalPoint = b2.Body.GetLocalPoint
local _math_sqrt = math.sqrt
function b2.World:NewRopeJoint(a, b, p1x, p1y, p2x, p2y, l, cc)
  if l == nil then
    local lx, ly = p1x - p2x, p1y - p2y
    l = _math_sqrt(lx*lx + ly*ly)
  end
  _Vec2_Set(_pt1, p1x, p1y)
  _Vec2_Set(_pt2, p2x, p2y)
  _body_GetLocalPoint(a, _pt1, _RopeJointDef_localAnchorA)
  _body_GetLocalPoint(b, _pt2, _RopeJointDef_localAnchorB)
  _RopeJointDef.bodyA = a
  _RopeJointDef.bodyB = b
  _RopeJointDef.maxLength = l
  _RopeJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _RopeJointDef)
end

local _PulleyJointDef = b2.PulleyJointDef()
local _PulleyJointDef_Initialize = _PulleyJointDef.Initialize
function b2.World:NewPulleyJoint(a, b, p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y, ratio, cc)
  _Vec2_Set(_pt1, p1x, p1y)
  _Vec2_Set(_pt2, p2x, p2y)
  _Vec2_Set(_pt3, p3x, p3y)
  _Vec2_Set(_pt4, p4x, p4y)
  _PulleyJointDef_Initialize(_PulleyJointDef, a, b, _pt1, _pt2, _pt3, _pt4, ratio or 1)
  _PulleyJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _PulleyJointDef)
end

local _GearJointDef = b2.GearJointDef()
function b2.World:NewGearJoint(a, b, j1, j2, ratio, cc)
  _GearJointDef.bodyA = a
  _GearJointDef.bodyB = b
  _GearJointDef.joint1 = j1
  _GearJointDef.joint2 = j2
  _GearJointDef.ratio = ratio or 1
  _GearJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _GearJointDef)
end

local _WeldJointDef = b2.WeldJointDef()
local _WeldJointDef_Initialize = _WeldJointDef.Initialize
function b2.World:NewWeldJoint(a, b, p1x, p1y, cc)
  _Vec2_Set(_pt1, p1x, p1y)
  _WeldJointDef_Initialize(_WeldJointDef, a, b, _pt1)
  _WeldJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _WeldJointDef)
end

local _FrictionJointDef = b2.FrictionJointDef()
local _FrictionJointDef_Initialize = _FrictionJointDef.Initialize
function b2.World:NewFrictionJoint(a, b, p1x, p1y, cc)
  _Vec2_Set(_pt1, p1x, p1y)
  _FrictionJointDef_Initialize(_FrictionJointDef, a, b, _pt1)
  _FrictionJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _FrictionJointDef)
end

local _WheelJointDef = b2.WheelJointDef()
local _WheelJointDef_Initialize = _WheelJointDef.Initialize
function b2.World:NewWheelJoint(a, b, p1x, p1y, ax, ay, cc)
  _Vec2_Set(_pt1, p1x, p1y)
  _Vec2_Set(_pt2, ax, ay)
  _WheelJointDef_Initialize(_WheelJointDef, a, b, _pt1, _pt2)
  _WheelJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _WheelJointDef)
end

local _MouseJointDef = b2.MouseJointDef()
local _MouseJointDef_target = _MouseJointDef.target
function b2.World:NewMouseJoint(a, b, x, y, mf, cc)
  _MouseJointDef.bodyA = a
  _MouseJointDef.bodyB = b
  _MouseJointDef.maxForce = mf
  _Vec2_Set(_MouseJointDef_target, x, y)
  _MouseJointDef.collideConnected = cc or false
  return _World_CreateJoint(self, _MouseJointDef)
end