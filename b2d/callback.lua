-- Collision callbacks by reusing intermediate definitions

-- Listeners
local _listeners = {}

-- Supported callbacks
local _ctypes =
{
  BeginContact = true,
  EndContact = true,
  PreSolve = true,
  PostSolve = true
}

local _ContactListener = b2.ContactListener
local _World_SetContactListener = b2.World.SetContactListener

function b2.World:SetCallback(n, func)
  if not _ctypes[n] then
    error("unsupported callback: " .. n)
  end
  local cl = _listeners[self] or _ContactListener()
  _listeners[self] = cl
  -- note: first argument is the contact listener
  local f = nil
  if func ~= nil then
    f = function(cl, ...) func(...) end
  end
  cl[n] = f
  _World_SetContactListener(self, cl)
end

function b2.World:GetCallback(n)
  local cl = _listeners[self]
  if cl then
    return cl[n]
  end
end

function b2.World:ResetCallbacks()
  _listeners[self] = nil
  _World_SetContactListener(self, nil)
end