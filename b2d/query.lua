-- Queries by reusing intermediate definitions

-- Vec2
local _pt1 = b2.Vec2(0, 0)
local _Vec2_Set = b2.Vec2.Set

-- AABB
local _aabb = b2.AABB()
local _aabb_lowerBound = _aabb.lowerBound
local _aabb_upperBound = _aabb.upperBound

-- Transform
local _tf = b2.Transform()
local _tf_p = _tf.p
local _tf_q = _tf.q
local _Body_GetTransformXYCS = b2.Body.GetTransformXYCS

-- QueryCallback
local _query = nil
local _qcb = b2.QueryCallback()

local function _ReportFixture(_qcb, f)
  _query[#_query + 1] = f
  return true
end

local _toi = b2.linearSlop/2
local _Fixture_TestPoint = b2.Fixture.TestPoint
local function _ReportFixturePt(_qcb, f)
  if _Fixture_TestPoint(f, _pt1) then
    _query[#_query + 1] = f
  end
  return true
end

-- World
local _World_QueryAABB = b2.World.QueryAABB
function b2.World:QueryLTRB(l, t, r, b, q)
  q = q or {}
  -- range
  _Vec2_Set(_aabb_lowerBound, l, t)
  _Vec2_Set(_aabb_upperBound, r, b)
  -- result
  _query = q
  _qcb.ReportFixture = _ReportFixture
  -- query
  _World_QueryAABB(self, _qcb, _aabb)
  return q
end

function b2.World:QueryXY(x, y, q)
  q = q or {}
  -- range
  _Vec2_Set(_aabb_lowerBound, x - _toi, y - _toi)
  _Vec2_Set(_aabb_upperBound, x + _toi, y + _toi)
  -- point
  _Vec2_Set(_pt1, x, y)
  -- result
  _query = q
  _qcb.ReportFixture = _ReportFixturePt
  -- run query
  _World_QueryAABB(self, _qcb, _aabb)
	return q
end

local _Body_GetFixtureList = b2.Body.GetFixtureList
local _Fixture_GetNext = b2.Fixture.GetNext
function b2.Body:TestPointXY(x, y)
  -- query point
  _Vec2_Set(_pt1, x, y)
  -- iterate fixtures
  local f = _Body_GetFixtureList(self)
  while f do
    if _Fixture_TestPoint(f, _pt1) then
      return true
    end
    f = _Fixture_GetNext(f)
  end
  return false
end

local _Body_GetContactList = b2.Body.GetContactList
local _Contact_IsTouching = b2.Contact.IsTouching

function b2.Body:IsTouching(other)
  -- iterate possible collisions
  local ce = _Body_GetContactList(self)
  while ce do
    if ce.other == other then
      if _Contact_IsTouching(ce.contact) then
        return true
      end
    end
    ce = ce.next
  end
  return false
end