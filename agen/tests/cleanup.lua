display:create("cleanup", 800, 600, 32, true)

display.terminal = require("utils.log.terminal")
display.terminal.immediate = false

local loads = 0
local unloads = 0
local paused = true
local file = "utils/agen/tests/smallimg.png"
local method = "display.images"

-- using display.images
display.images = require("utils.agen.images")
-- using the method "ooh baby, I like it raw"
local image = Image()
function loadFile(fn)
  if method == 'display.images' then
    return display.images.load(fn) -- display.images
  else
    return image:load(fn) -- raw
  end
end
function unloadFile(fn)
  if method == 'display.images' then
    return display.images.unload(fn) -- display.images
  else
    return image:unload() -- raw
  end
end

timer = Timer()
timer:start(16, true)
function timer:on_tick()
  if not paused then
    if display.images.load(file) then
      loads = loads + 1
    end
    if display.images.unload(file) then
      unloads = unloads + 1
    end
  end
  display.terminal.trace('loads/unloads', loads .. '/' .. unloads)
  display.terminal.trace('file (RMB)', file)
  display.terminal.trace('method (MMB)', method)
  display.terminal.trace('paused (LMB)', paused)
  --display.terminal.trace('loaded', display.images.loaded)
  --display.terminal.trace('size', display.images.size)
  collectgarbage("collect")
  
  display.terminal.redraw()
end

function memusage()
  local handle = io.popen('tasklist /fi "Imagename eq Agen.exe"')
  local result = handle:read("*a")
  handle:close()
  result = string.match(result, ' ([^%s]-) K') -- remove this for verbose output
  result = result or 'n/a'
  display.terminal.printf('\nreloads: %s', loads)
  display.terminal.printf('process: %s K', result)
  display.terminal.printf('lua: %d K', collectgarbage("count"))
end

display.terminal.print('test for memory leaks when reloading images')
display.terminal.print('loads/unloads the same image multiple times')
display.terminal.print('uses "tasklist" which is not available on XP')
display.terminal.print('pause for memory usage report')
memusage()
  
function mouse:on_press(b)
  if b == MBUTTON_RIGHT then
    if file == "utils/agen/tests/largeimg.png" then
      file = "utils/agen/tests/smallimg.png"
    else
      file = "utils/agen/tests/largeimg.png"
    end
  elseif b == MBUTTON_MIDDLE then
    if method == "raw" then
      method = "display.images"
    else
      method = "raw"
    end
  else
    paused = not paused
    if paused then
      memusage()
    end
  end
end