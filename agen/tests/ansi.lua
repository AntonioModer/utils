display:create("terminal test", 1000, 600, 32, true, 30, false, 4)

terminal = require('utils.log.terminal')
terminal.immediate = false
print = terminal.print

ansi = require('utils.agen.ansi')

input = {}

local function stroke(k)
  -- keyboard state
  local i = 1
  if keyboard:is_down(KEY_LSHIFT) or keyboard:is_down(KEY_RSHIFT) then
    i = 2
  elseif keyboard:is_down(KEY_LCTRL) or keyboard:is_down(KEY_RCTRL) then
    i = 3
  elseif keyboard:is_down(KEY_LALT) or keyboard:is_down(KEY_RALT) then
    i = 4
  end
  -- input
  local b = ansi[k][i]
  if b then
    if b == 8 or b == 127 then
      table.remove(input)
    elseif b == 9 then
      table.insert(input, '\t')
    elseif b >= 10 and b <= 13 then
      local sz = table.concat(input)
      local r, e = loadstring(sz)
      if r then
        r, e = pcall(r)
      end
      if e then
        terminal.print(e)
      end
      input = {}
    elseif b <= 126 then
      local c = string.char(b)
      if b < 32 then
        c = string.char(94, b + 64)
      end
      table.insert(input, c)
    end
  end
end

function keyboard:on_press(k)
  stroke(k)
end

timer = Timer()
function timer:on_tick()
  terminal.trace("input", table.concat(input) .. '_')
  terminal.redraw()
end
timer:start(16, true)