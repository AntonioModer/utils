local keys = require("utils.agen.keys")

-- reverse lookup
local rkeys = {}
for id, label in pairs(keys) do
  assert(rkeys[label] == nil)
  rkeys[label] = id
end

local actionmap = {}

local _devices = {}
local _actions = {}
local _active = {}

--- Adds device
-- @param id unique device id
-- @param d device object
function actionmap.addDevice(id, d)
  assert(_devices[id] == nil, "device id already exists")
  assert(type(d) == "userdata", "device must be an object")
  _devices[id] = d
  _actions[id] = _actions[id] or {}
  _active[id] = _active[id] or {}
end

--- Gets device
-- @param id device id
-- @return device object
function actionmap.getDevice(id)
  return _devices[id]
end

--- Checks if the device has any mappings
-- @param id device id
-- @return true if there are any mappings
function actionmap.isMapped(id)
  return _actions[id] and next(_actions[id]) ~= nil
end

--- Updates devices and triggers callbacks
function actionmap.process()
  -- todo: inefficient
  -- for each device
  for id, device in pairs(_devices) do
    -- for each mapping
    for _, map in pairs(_actions[id]) do
      local is = true
      for j = 1, #map do
        if not device:is_down(map[j]) then
          is = false
          break
        end
      end
      local was = _active[id][map]
      _active[id][map] = is
      
      -- are all buttons for this mapping pressed?
      if is then
        -- just pressed?
        if not was then
          actionmap.beginAction(id, map.a)
        end
      else
        -- just released?
        if was then
          actionmap.endAction(id, map.a)
        end
      end
    end
  end
end

--- Adds action for device
function actionmap.addAction(id, action, sz)
  -- OR
  for l in string.gmatch(sz, "[^|%s]+") do
    -- AND
    local map = {}
    for b in string.gmatch(l, "[^%+%s]+") do
      -- legit key?
      if rkeys[b] then
        table.insert(map, b)
      end
    end
    if #map > 0 then
      -- action
      map.a = action
      -- buttons
      map.b = table.concat(map, "+")
      -- deserialize
      for i = 1, #map do
        local b = map[i]
        map[i] = rkeys[b]
      end
      table.sort(map)
      -- store using unique map ids, overwriting duplicates
      local mid = string.format("%s:%s", map.a, map.b)
      _actions[id] = _actions[id] or {}
      _actions[id][mid] = map
    end
  end
end

--- Gets action for device
function actionmap.getAction(id, action)
  local maps = {}
  for _, map in pairs(_actions[id]) do
    if map.a == action then
      -- AND
      table.insert(maps, map.b)
    end
  end
  table.sort(maps)
  -- OR
  return table.concat(maps, "|")
end

--- Adds mapping for device
function actionmap.addMapping(sz, id)
  -- get id from file
  local _id = string.match(sz .. "\n", "[%s]*#(.-)\n")
  id = id or _id
  if not id then
    return false
  end
  sz = string.gsub(sz .. "\n", "([%s]*#.-)\n", "")
  for line in string.gmatch(sz, "(.-)\n") do
    for a, b in string.gmatch(line .. ",", "(.-):(.-),") do
      actionmap.addAction(id, a, b)
    end
  end
  return true
end

--- Gets mapping for device
function actionmap.getMapping(id)
  local a = {}
  for _, map in pairs(_actions[id]) do
    a[map.a] = true
  end
  local b = {}
  for _a in pairs(a) do
    local map = actionmap.getAction(id, _a)
    table.insert(b, string.format("%s:%s", _a, map))
  end
  return string.format("#%s\n%s", id, table.concat(b, ",\n"))
end

--- Removes mapping for device
function actionmap.removeMapping(id)
  if _actions[id] then
    _actions[id] = {}
  end
end

--- Loads mapping from file
function actionmap.loadMapping(fn, id)
  local f = io.open(fn, "r")
  if f == nil then
    return false
  end
  local map = f:read("*a")
  f:close()
  return actionmap.addMapping(map, id)
end

--- Saves mapping to file
function actionmap.saveMapping(fn, id)
  local map = actionmap.getMapping(id)
  if not map then
    return
  end
  local f = io.open(fn, "w")
  if f == nil then
    return false
  end
  f:write(map)
  f:close()
  return true
end

function actionmap.beginAction(id, action)
  
end

function actionmap.endAction(id, action)

end

return actionmap