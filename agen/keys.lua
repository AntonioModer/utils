-- string labels used for mapping serialization

return
{
-- function
[1]='escape',
[2]='f1',
[3]='f2',
[4]='f3',
[5]='f4',
[6]='f5',
[7]='f6',
[8]='f7',
[9]='f8',
[10]='f9',
[11]='f10',
[12]='f11',
[13]='f12',
[14]='f13',
[15]='f14',
[16]='f15',
-- standard
[17]='1',
[18]='2',
[19]='3',
[20]='4',
[21]='5',
[22]='6',
[23]='7',
[24]='8',
[25]='9',
[26]='0',
[27]='minus',
[28]='equals',
[29]='back',
[30]='tab',
[31]='q',
[32]='w',
[33]='e',
[34]='r',
[35]='t',
[36]='y',
[37]='u',
[38]='i',
[39]='o',
[40]='p',
[41]='lsqb',
[42]='rsqb',
[43]='bslash',
[44]='caps',
[45]='a',
[46]='s',
[47]='d',
[48]='f',
[49]='g',
[50]='h',
[51]='j',
[52]='k',
[53]='l',
[54]='colon',
[55]='apos',
[56]='enter',
[57]='lshift',
[58]='z',
[59]='x',
[60]='c',
[61]='v',
[62]='b',
[63]='n',
[64]='m',
[65]='comma',
[66]='dot',
[67]='slash',
-- command
[68]='rshift',
[69]='lctrl',
[70]='lwin',
[71]='lalt',
[72]='space',
[73]='ralt',
[74]='rwin',
[75]='rmenu',
[76]='rctrl',
[77]='pscreen',
[78]='slock',
[79]='pause',
[80]='insert',
[81]='home',
[82]='pgup',
[83]='delete',
[84]='end',
[85]='pgdown',
[86]='up',
[87]='left',
[88]='down',
[89]='right',
-- numeric keypad
[90]='nlock',
[91]='ndivide',
[92]='nmultiply',
[93]='nminus',
[94]='n1',
[95]='n2',
[96]='n3',
[97]='n4',
[98]='n5',
[99]='n6',
[100]='n7',
[101]='n8',
[102]='n9',
[103]='n0',
[104]='ndelete',
[105]='nplus',
[106]='nenter',

-- mouse
[108]='mleft',
[109]='mmiddle',
[110]='mright',
[111]='mx',

-- joystick
-- buttons
[112]='j1',
[113]='j2',
[114]='j3',
[115]='j4',
[116]='j5',
[117]='j6',
[118]='j7',
[119]='j8',
[120]='j9',
[121]='j10',
[122]='j11',
[123]='j12',
[124]='j13',
[125]='j14',
[126]='j15',
[127]='j16',
-- hat switch
[128]='jhup',
[129]='jhright',
[130]='jhdown',
[131]='jhleft',
-- dpad
[132]='jup',
[133]='jdown',
[134]='jleft',
[135]='jright',
-- rotation
[136]='jzleft',
[137]='jzright'
}