local byte = string.byte
local sub = string.sub
local format = string.format

local images = {}

-- enable or disable image loading
images.enabled = true
images.loaded = 0
images.size = 0

local lookup = {}

images.lookup = lookup

--- Finds image from filename
-- @param fn filename
-- @return image
function images.find(fn)
  return lookup[fn]
end

--- Finds filename from image
-- @param i image
-- @return id
function images.filename(i)
  for n, i2 in pairs(lookup) do
    if i == i2 then
      return n
    end
  end
end

--- Load an image from filename
-- @param fn filename
-- @return loaded image
function images.load(fn)
  if images.enabled ~= true then
    return
  end
  local i = lookup[fn]
  -- already loaded?
  if i == nil then
    i = Image()
    if i:load(fn) == false then
      return
    end
    -- reference
    lookup[fn] = i
    local s = i.width*i.height
    images.loaded = images.loaded + 1
    images.size = images.size + s
  end
  return i
end

--- Loads a spritesheet image from filename
-- @param fn filename
-- @param c columns
-- @param r rows
-- @return image object
function images.load_sheet(fn, c, r)
  if images.enabled ~= true then
    return
  end
  assert(c, "number of columns is nil")
  assert(r, "number of rows is nil")
  local image = Image()
  if image:load(fn) == false then
    return
  end
  -- tile dimensions from cols and rows
  local w = image.width/c
  local h = image.height/r
  for x = 0, c - 1 do
    for y = 0, r - 1 do
      local i =
      {
        texture = image,
        -- position in the spritesheet
        x = x*w,
        y = y*h,
        -- tile dimensions
        width = w,
        height = h
      }
      -- spritesheet tiles are indexed as follows: 
      -- "filename.1", "filename.2", "filename.3", etc
      local fn2 = format("%s.%d", fn, x + y*c)
      assert(lookup[fn2] == nil, "image id already exists")
      --images.add(n, i)
      lookup[fn2] = i
    end
  end
  local s = i.width*i.height
  images.loaded = images.loaded + 1
  images.size = images.size + s
  return image
end

--- Loads a texture atlas from filename
--- Requires LuaXML
-- @param fn filename
-- @return image object
function images.load_atlas(fn)
  if images.enabled ~= true then
    return
  end
  local x = xml.load(fn)
  local atlas = xml.find(x, 'TextureAtlas')
  if atlas == nil then
    return
  end
  local image = Image()
  if image:load(atlas.imagePath) == false then
    return
  end
  -- add entry for each sprite tag
  for _, v in ipairs(atlas) do
    if type(v) == 'table' then
      if xml.tag(v) == 'sprite' then
        -- add to images
        local xoff = -(v.wOrig/2 - (v.xoff + (v.width/2)))
        local yoff = v.hOrig/2 - (v.yoff + (v.height/2))
        local i =
        {
          texture = image,
          -- position in the spritesheet
          x = v.x,
          y = v.y,
          -- tile dimensions
          width = v.width,
          height = v.height,
          -- drawing offset
          ox = xoff,
          oy = yoff
        }
        -- spritesheet tiles are indexed as follows: 
        -- "atlas.name1", "atlas.name2", "atlas.name3", etc
        local n = format("%s.%s", fn, v.name)
        assert(lookup[n] == nil, "image id already exists")
        lookup[n] = i
      end
    end
  end
  local s = i.width*i.height
  images.loaded = images.loaded + 1
  images.size = images.size + s
  return image
end

--- Unloads an image
-- @param fn filename
function images.unload(fn)
  local i = lookup[fn]
  if i then
    local s = i.width*i.height
    if i:unload() == true then
      -- dereference
      lookup[fn] = nil
      images.loaded = images.loaded - 1
      images.size = images.size - s
      return true
    end
  end
  return false
end

--- Unloads all images
function images.unload_all()
  for i, v in pairs(lookup) do
    images.unload(i)
  end
end

return images