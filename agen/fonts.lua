local codes = require("utils.lang.codes")

local fonts = {}
local _all = {}

function fonts.get_codes(...)
  local c = {}
  local n = select('#', ...)
  if n > 0 then
    for i = 1, n do
      local a = select(i, ...)
      if a ~= nil then
        assert(type(a) == "string", "block name must be a string")
        local b = codes[a]
        if b == nil then
          assert(false, "unknown block: " .. a)
        end
        for j = b[1], b[2] do
          assert(table.find(c, j) == nil, "code already included")
          table.insert(c, j)
        end
      end
    end
  end
  return c
end

function fonts.set_encoding(esz)
  assert(esz, "Encoding is nil")
  if _all[esz] == nil then
    _all[esz] = {}
  end
  fonts.current = _all[esz]
  fonts.currentsz = esz
end

function fonts.get_encoding()
  return fonts.currentsz
end

function fonts.load_func_encoding(esz, func, fn, s, c)
  assert(esz, "Encoding is nil")
  assert(fn, "Font filename is nil")
  if _all[esz] == nil then
    _all[esz] = {}
  end
  local e = _all[esz]
  local i = e[fn]
  -- already loaded?
  if i == nil then
    i = Font()
    local l = false
    -- don't load codes for bmfonts
    local func2 = func .. '_codes'
    if c and Font[func2] then
      l = i[func2](i, fn, s, c)
    else
      l = i[func](i, fn, s)
    end
    -- loaded successfully?
    if l == false then
      return
    end
    -- index by filename
    e[fn] = i
  end
  return i
end

function fonts.load_file_encoding(esz, fn, s, c)
  local ext = string.sub(fn, -3, -1)
  if ext == "fnt" and Font.load_bmfont then
    return fonts.load_func_encoding(esz, 'load_bmfont', fn, s, c)
  else
    return fonts.load_func_encoding(esz, 'load_file', fn, s, c)
  end
end

function fonts.load_system_encoding(esz, fn, s, c)
  return fonts.load_func_encoding(esz, 'load_system', fn, s, c)
end

function fonts.load_file(fn, s, c)
  return fonts.load_file_encoding(fonts.currentsz, fn, s, c)
end

function fonts.load_system(f, s, c)
  return fonts.load_system_encoding(fonts.currentsz, f, s, c)
end

function fonts.unload_font_encoding(esz, f)
  assert(f, "Font filename is nil")
  local e = _all[esz]
  local i = e[f]
  if i then
    if i:unload() == true then
      e[f] = nil
      return true
    end
  end
  return false
end

function fonts.unload_font(f)
  return fonts.unload_font_encoding(fonts.currentsz, f)
end

function fonts.unload_fonts_encoding(esz)
  for i in pairs(_all[esz]) do
    fonts.unload_font(i)
  end
end

function fonts.unload_fonts()
  for esz in pairs(_all) do
    fonts.unload_fonts_encoding(esz)
  end
end

function fonts.set_font_name_encoding(esz, f, n)
  local e = _all[esz]
  assert(f ~= nil, "Font is nil")
  assert(n ~= nil, "Font name is nil")
  assert(e[n] == nil, "Font name already exists")
  for i, v in pairs(e) do
    if v == f then
      e[n] = f
      e[i] = nil
      break
    end
  end
end

function fonts.set_font_name(f, n)
  fonts.set_font_name_encoding(fonts.currentsz, f, n)
end

function fonts.find_font_encoding(esz, f, n)
  assert(f, "Font filename is nil")
  local e = _all[esz]
  if e then
    return e[f]
  end
end

function fonts.find_font(f)
  return fonts.find_font_encoding(fonts.currentsz, f)
end

return fonts